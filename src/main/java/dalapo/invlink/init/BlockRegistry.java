package dalapo.invlink.init;

import java.util.ArrayList;
import java.util.List;

import dalapo.invlink.auxiliary.ISpecialItem;
import dalapo.invlink.block.BlockBase;
import dalapo.invlink.block.BlockDirectionalTile;
import dalapo.invlink.block.BlockPipeSection;
import dalapo.invlink.item.ItemBlockSubtypes;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraft.item.ItemBlock;
import net.minecraftforge.event.RegistryEvent;

public class BlockRegistry
{
	private static final List<BlockBase> blocks = new ArrayList<>();
	
	public static final BlockPipeSection pipe = new BlockPipeSection("pipe");
	public static final BlockDirectionalTile teleportPipe = (BlockDirectionalTile)new BlockDirectionalTile(Material.IRON, "teleport").setCreativeTab(CreativeTabs.TRANSPORTATION);
	public static final BlockDirectionalTile netinserter = (BlockDirectionalTile)new BlockDirectionalTile(Material.WOOD, "netinserter").setCreativeTab(CreativeTabs.TRANSPORTATION);
	public static final BlockDirectionalTile retriever = (BlockDirectionalTile)new BlockDirectionalTile(Material.WOOD, "retriever").setCreativeTab(CreativeTabs.TRANSPORTATION);
	public static final BlockDirectionalTile router = (BlockDirectionalTile)new BlockDirectionalTile(Material.IRON, "router").setCreativeTab(CreativeTabs.TRANSPORTATION);
	
	public static void addBlock(BlockBase block)
	{
		blocks.add(block);
	}
	public static void register(RegistryEvent.Register<Block> evt)
	{
		blocks.forEach(block -> evt.getRegistry().register(block));
	}
	public static void registerItemBlocks(RegistryEvent.Register<Item> evt)
	{
		blocks.forEach(block -> {
			if (block instanceof ISpecialItem)
			{
				evt.getRegistry().register(new ItemBlockSubtypes(block).setRegistryName(block.getRegistryName()));
			}
			else
			{
				evt.getRegistry().register(new ItemBlock(block).setRegistryName(block.getRegistryName()));
			}
		});
	}
	public static void initModels()
	{
		blocks.forEach(block -> block.initModel());
	}
	public static void registerItemModels()
	{
		blocks.forEach(block -> block.initItemModel());
	}
}