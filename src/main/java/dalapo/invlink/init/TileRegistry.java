package dalapo.invlink.init;

import dalapo.invlink.reference.NameList;
import dalapo.invlink.tileentity.*;
import dalapo.invlink.client.render.tesr.*;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.client.registry.ClientRegistry;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class TileRegistry
{
	private static void registerTile(Class<? extends TileEntity> tile, String tileName)
	{
		GameRegistry.registerTileEntity(tile, new ResourceLocation(NameList.MOD_ID + ":" + tileName));
	}
	
	public static TileEntityBase getTileFromName(String name)
	{
		switch (name)
		{
		case "pipe":
			return new TileEntityItemPipe();
		case "detector":
			return new TileEntityDetectorPipe();
		case "netinserter":
			return new TileEntityNotTransposer();
		case "retriever":
			return new TileEntityNotRetriever();
		case "teleport":
			return new TileEntityTeleportPipe();
		case "router":
			return new TileEntityRouter();
		default:
			throw new IllegalArgumentException("Tried to fetch a nonexistent tile entity: " + name);
		}
	}
	
	public static void registerTiles()
	{
		registerTile(TileEntityItemPipe.class, "pipe");
		registerTile(TileEntityNotTransposer.class, "netinserter");
		registerTile(TileEntityNotRetriever.class, "retriever");
		registerTile(TileEntityDetectorPipe.class, "detector");
		registerTile(TileEntityTeleportPipe.class, "teleport");
		registerTile(TileEntityRouter.class, "router");
	}
	
	@SideOnly(Side.CLIENT)
	public static void registerTESRs()
	{
		ClientRegistry.bindTileEntitySpecialRenderer(TileEntityItemPipe.class, new TesrItemPipe());
	}
}