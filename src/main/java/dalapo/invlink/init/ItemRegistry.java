package dalapo.invlink.init;

import java.util.ArrayList;
import java.util.List;

import dalapo.invlink.item.*;
import net.minecraft.item.Item;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class ItemRegistry
{
	private static List<ItemBase> items = new ArrayList<>();
	
	public static final ItemNetworkDebugger debugger = new ItemNetworkDebugger("debug");
	public static final ItemNodeDebugger nodedebug = new ItemNodeDebugger("nodedebug");
	public static final ItemUpgrade upgrade = new ItemUpgrade("upgrade");
	public static final ItemLinker linker = new ItemLinker("linker");
	
	public static void addItem(ItemBase item)
	{
		items.add(item);
	}
	
	public static void register(RegistryEvent.Register<Item> evt)
	{
		items.forEach(item -> evt.getRegistry().register(item));
	}
	
	@SideOnly(Side.CLIENT)
	public static void initModels()
	{
		items.forEach(item -> item.initModel());
	}
}