package dalapo.invlink;

import java.io.File;

import dalapo.invlink.client.gui.InvLinkGuiHandler;
import dalapo.invlink.init.BlockRegistry;
import dalapo.invlink.init.ItemRegistry;
import dalapo.invlink.init.TileRegistry;
import dalapo.invlink.packet.PacketHandler;
import net.minecraft.block.Block;
import net.minecraft.item.Item;
import net.minecraftforge.common.config.Configuration;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.network.NetworkRegistry;

public class CommonProxy
{
	public static Configuration config;
	public void preInit(FMLPreInitializationEvent evt)
	{
		File directory = evt.getModConfigurationDirectory();
		config = new Configuration(new File(directory.getPath(), "invlink.cfg"));
		InvLinkConfigManager.readConfig();
		PacketHandler.registerMessages("invlink");
	}
	
	public void init(FMLInitializationEvent evt)
	{
		NetworkRegistry.INSTANCE.registerGuiHandler(InventoryLink.instance, new InvLinkGuiHandler());
	}
	
	public void postInit(FMLPostInitializationEvent evt)
	{
		if (config.hasChanged()) config.save();
	}
	
	@SubscribeEvent
	public void registerBlocks(RegistryEvent.Register<Block> evt)
	{
		BlockRegistry.register(evt);
		TileRegistry.registerTiles();
	}
	
	@SubscribeEvent
	public void registerItems(RegistryEvent.Register<Item> evt)
	{
		ItemRegistry.register(evt);
		BlockRegistry.registerItemBlocks(evt);
	}
}