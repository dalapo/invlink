package dalapo.invlink.reference;

import net.minecraft.block.properties.PropertyDirection;
import net.minecraft.block.properties.PropertyEnum;
import net.minecraft.util.IStringSerializable;

public class StateList
{
	private StateList() {}
	
	public static final PropertyDirection directions = PropertyDirection.create("facing");
	
	public static final PropertyEnum<PipeType> pipetypes = PropertyEnum.create("pipetype", PipeType.class);
	
	public static enum PipeType implements IStringSerializable
	{
		NORMAL("normal"),
		DETECTOR_OFF("detector_off"),
		DETECTOR_ON("detector_on");

		private final String name;
		
		private PipeType(String name)
		{
			this.name = name;
		}
		@Override
		public String getName()
		{
			return name;
		}
		
		public static PipeType getFromMeta(int meta)
		{
			return values()[meta];
		}
	}
}