package dalapo.invlink;

import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.Instance;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;

import java.util.Random;

import org.apache.logging.log4j.Logger;

import dalapo.invlink.reference.NameList;

// Hello. If you are reading this, you are likely on GitLab looking through the Inventory Link code.
// Within is a birds' nest of disorganized methods which I'm frankly too afraid to try to fix at this point.
// Study at your own risk.
@Mod(name=NameList.MOD_NAME, modid=NameList.MOD_ID, version=NameList.MOD_VERSION)
public class InventoryLink
{
	public static Random random = new Random();
	
	@Instance
	public static InventoryLink instance;
	
	public static Logger logger;
	
	@SidedProxy(clientSide="dalapo.invlink.ClientProxy", serverSide="dalapo.invlink.CommonProxy")
	public static CommonProxy proxy;
	
	@Mod.EventHandler
	public void preInit(FMLPreInitializationEvent evt)
	{
		proxy.preInit(evt);
		MinecraftForge.EVENT_BUS.register(proxy);
		MinecraftForge.EVENT_BUS.register(InvLinkEventHandler.instance);
		logger = evt.getModLog();
	}
	
	@Mod.EventHandler
	public void init(FMLInitializationEvent evt)
	{
		proxy.init(evt);
		dalapo.invlink.helper.Logger.info("Want some pipes? Come down to Inventory Link Transportation Systems Ltd. and let us help with that!");
	}
	
	@Mod.EventHandler
	public void postInit(FMLPostInitializationEvent evt)
	{
		proxy.postInit(evt);
	}
}