package dalapo.invlink;

import dalapo.invlink.auxiliary.BlockUpdateListener;
import dalapo.invlink.auxiliary.RouterTileRegistry;
import dalapo.invlink.helper.Logger;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockPos.MutableBlockPos;
import net.minecraftforge.event.entity.item.ItemEvent;
import net.minecraftforge.event.entity.player.PlayerDropsEvent;
import net.minecraftforge.event.world.BlockEvent.BreakEvent;
import net.minecraftforge.event.world.BlockEvent.EntityPlaceEvent;
import net.minecraftforge.event.world.BlockEvent.NeighborNotifyEvent;
import net.minecraftforge.event.world.BlockEvent.PlaceEvent;
import net.minecraftforge.event.world.ChunkEvent;
import net.minecraftforge.event.world.WorldEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class InvLinkEventHandler
{
	public static final InvLinkEventHandler instance = new InvLinkEventHandler();
	
	private InvLinkEventHandler() {}
	
	@SubscribeEvent
	public void recalcRouterConnections(BreakEvent evt)
	{
		RouterTileRegistry.instance.handleBreak(evt.getPos());
	}
	
//	@SubscribeEvent
//	public void addToRouter(EntityPlaceEvent evt)
//	{
//		for (EnumFacing f : EnumFacing.VALUES)
//		{
//			if (RouterTileRegistry.instance.routerMap.containsKey(evt.getPos().offset(f)))
//			{
//				RouterTileRegistry.instance.handlePlace(evt.getPos().offset(f));
//			}
//		}
//	}
////	@SubscribeEvent
////	public void extendDeadPlayerInventory(PlayerDropsEvent evt)
////	{
////		evt.getDrops().forEach(ei -> ei.lifespan = 36000);
////	}
}