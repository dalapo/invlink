package dalapo.invlink.helper;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.BufferBuilder;
import net.minecraft.client.renderer.block.model.ItemCameraTransforms;
import net.minecraft.item.ItemStack;
import net.minecraftforge.fml.relauncher.SideOnly;
import net.minecraftforge.fml.relauncher.Side;

@SideOnly(Side.CLIENT)
public class RenderHelper
{
	private RenderHelper() {}
	
	public static void renderItemStack(ItemStack is)
	{
		Minecraft.getMinecraft().getRenderItem().renderItem(is, ItemCameraTransforms.TransformType.NONE);
	}
	
	public static void drawLine(BufferBuilder builder, Point a, Point b)
	{
		builder.pos(a.x, a.y, a.z).endVertex();
		builder.pos(b.x, b.y, b.z).endVertex();
	}
	
	public static class Point
	{
		double x;
		double y;
		double z;
		
		public Point(double i, double j, double k)
		{
			x = i;
			y = j;
			z = k;
		}
		
		public void scale(double i, double j, double k)
		{
			x *= i;
			y *= j;
			z *= k;
		}
		
		public double getField(int i)
		{
			switch (i)
			{
			case 0:
				return x;
			case 1:
				return y;
			case 2:
				return z;
				default:
					return 0.0;
			}
		}
	}
}