package dalapo.invlink.helper;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;

public class CollectionHelper
{
	private CollectionHelper() {}
	
	public static <T> void removeEntries(Map<T, ?> map, Predicate<T> condition)
	{
		List<T> toRemove = new ArrayList<>();
		for (T t : map.keySet())
		{
			if (condition.test(t)) toRemove.add(t); // Cannot remove directly without causing a CME
		}
		for (T t : toRemove)
		{
			map.remove(t);
		}
	}
}