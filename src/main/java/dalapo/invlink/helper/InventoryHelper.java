package dalapo.invlink.helper;

import java.util.function.Predicate;

import dalapo.invlink.auxiliary.BackstuffInventory;
import net.minecraft.item.ItemStack;
import net.minecraftforge.items.IItemHandler;

public class InventoryHelper
{
	private InventoryHelper() {}
	
	/**
	 * Tries to insert as much of the given ItemStack into the given inventory as possible.
	 * @param inv
	 * @param itemstack
	 * @param simulate
	 * @return The remainder of the ItemStack that couldn't be inserted.
	 */
	public static ItemStack tryInsertItem(IItemHandler inv, ItemStack itemstack, boolean simulate)
	{
		if (itemstack.isEmpty()) return ItemStack.EMPTY;
		if (inv == null) return itemstack;
		for (int i=0; i<inv.getSlots(); i++)
		{
			itemstack = inv.insertItem(i, itemstack, simulate);
			if (itemstack.isEmpty()) break;
		}
		return itemstack;
	}
	
	private static int getFilterSlot(ItemStack is, IItemHandler filter)
	{
		for (int i=0; i<filter.getSlots(); i++)
		{
			if (is.isItemEqual(filter.getStackInSlot(i)) && is.getCount() >= filter.getStackInSlot(i).getCount()) return i;
		}
		return -1;
	}
	
	public static ItemStack findFirstItem(IItemHandler inv, Predicate<ItemStack> criteria)
	{
		for (int i=0; i<inv.getSlots(); i++)
		{
			ItemStack is = inv.getStackInSlot(i);
			if (criteria.test(is)) return is;
		}
		return ItemStack.EMPTY;
	}
	
	/**
	 * Extracts the first item from <code>inv</code> that matches <code>filter</code>, or the first item found if <code>filter</code> is empty.
	 * @param inv
	 * @param filter
	 * @return
	 */
	public static ItemStack extractFirstItem(IItemHandler inv, IItemHandler filter)
	{
		boolean emptyFilter = true;
		for (int i=0; i<filter.getSlots(); i++)
		{
			if (!filter.getStackInSlot(i).isEmpty())
			{
				emptyFilter = false;
				break;
			}
		}
		for (int i=0; i<inv.getSlots(); i++)
		{
			ItemStack is = inv.extractItem(i, inv.getSlotLimit(i), true);
			if (!is.isEmpty())
			{
				if (emptyFilter) return inv.extractItem(i, inv.getSlotLimit(i), false);
				else
				{
					int filterSlot = getFilterSlot(is, filter);
					if (filterSlot != -1) return inv.extractItem(i, filter.getStackInSlot(filterSlot).getCount(), false);
				}
			}
		}
		return ItemStack.EMPTY;
	}
	
	@Deprecated
	public static ItemStack findFirstItem(IItemHandler inv, IItemHandler filter, boolean extract)
	{
		boolean emptyFilter = true;
		for (int i=0; i<filter.getSlots(); i++)
		{
			if (!filter.getStackInSlot(i).isEmpty())
			{
				emptyFilter = false;
				break;
			}
		}
		if (emptyFilter) return findFirstItem(inv, extract);
		
		for (int i=0; i<inv.getSlots(); i++)
		{
			ItemStack is = inv.getStackInSlot(i);
			int slot = getFilterSlot(is, filter);
			if (!is.isEmpty() && slot != -1)
			{
				return inv.extractItem(i, filter.getStackInSlot(slot).getCount(), !extract);
			}
		}
		return ItemStack.EMPTY;
	}
	
	public static ItemStack findFirstItem(IItemHandler inv, boolean extract)
	{
		for (int i=0; i<inv.getSlots(); i++)
		{
			if (!inv.extractItem(i, inv.getSlotLimit(i), true).isEmpty()) return inv.extractItem(i, inv.getSlotLimit(i), !extract);
		}
		return ItemStack.EMPTY;
	}
	
	public static boolean isInventoryEmpty(IItemHandler inv)
	{
		return findFirstItem(inv, false).isEmpty();
	}
	
	public static boolean areItemStacksIdentical(ItemStack a, ItemStack b)
	{
		return (a.isItemEqual(b) && a.getCount() == b.getCount());
	}
	
	public static int getPriority(IItemHandler inv)
	{
		if (inv instanceof BackstuffInventory) return -1;
		return 0;
	}

	public static boolean isInventorySuitable(IItemHandler inventory, IItemHandler filter)
	{
		return !findFirstItem(inventory, filter, false).isEmpty();
	}
}