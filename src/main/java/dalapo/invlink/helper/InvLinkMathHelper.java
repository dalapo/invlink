package dalapo.invlink.helper;

import dalapo.invlink.network.PipeNode;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Vec3i;

public class InvLinkMathHelper
{
	private InvLinkMathHelper() {}
	
	/**
	 * Gets relative direction of block b to block a
	 * @param a
	 * @param b
	 * @return
	 */
	public static EnumFacing getRelativeDirection(BlockPos a, BlockPos b)
	{
		Vec3i vec = b.subtract(a);
		return EnumFacing.getFacingFromVector(vec.getX(), vec.getY(), vec.getZ());
	}
	
	/**
	 * Returns true if <code>x >= min && x < max </code>
	 * @param x
	 * @param min
	 * @param max
	 * @return
	 */
	public static boolean isInRange(int x, int min, int max)
	{
		return (x >= min && x < max);
	}
	
	/**
	 * Returns true if <code>x >= min && x <= max </code>
	 * @param x
	 * @param min
	 * @param max
	 * @return
	 */
	public static boolean isInRangeIncl(int x, int min, int max)
	{
		return (x >= min && x <= max);
	}
	
	public static double getAbsoluteDistance(BlockPos a, BlockPos b)
	{
		return a.getDistance(b.getX(), b.getY(), b.getZ());
	}
}