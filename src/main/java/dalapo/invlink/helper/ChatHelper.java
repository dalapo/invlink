package dalapo.invlink.helper;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TextComponentString;

public class ChatHelper
{
	public static void sendChatToPlayer(EntityPlayer ep, String msg)
	{
		if (ep.getEntityWorld().isRemote)
		{
			String[] parts = msg.split("\\n");
			for (int i=0; i<parts.length; i++)
			{
				ITextComponent chatPart = new TextComponentString(parts[i]);
				ep.sendMessage(chatPart);
			}
		}
	}
}