package dalapo.invlink.helper;

import java.util.LinkedList;

import dalapo.invlink.network.PipeNode;
import net.minecraft.util.EnumFacing;

public class NetworkHelper
{
	private NetworkHelper() {}
	
	public static LinkedList<EnumFacing> convertNodeList(LinkedList<PipeNode> in)
	{
		LinkedList<EnumFacing> list = new LinkedList<>();
		for (int i=1; i<in.size(); i++)
		{
			list.add(InvLinkMathHelper.getRelativeDirection(in.get(i-1).getTile().getPos(), in.get(i-1).getImmediateConnection(in.get(i)).getPos()));
		}
		list.add(null);
		return list;
	}
}