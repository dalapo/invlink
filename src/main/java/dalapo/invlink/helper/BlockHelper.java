package dalapo.invlink.helper;

import dalapo.invlink.reference.StateList;
import net.minecraft.block.state.IBlockState;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class BlockHelper
{
	private BlockHelper() {}
	
	public static void updateBlock(World world, BlockPos pos)
	{
		IBlockState state = world.getBlockState(pos);
		world.notifyBlockUpdate(pos, state, state, 3);
	}
	
	public static IBlockState rotateBlock(IBlockState stateIn)
	{
		EnumFacing dir = stateIn.getValue(StateList.directions);
		return stateIn.withProperty(StateList.directions, EnumFacing.getFront((dir.getIndex() + 1) % 6));
	}
	
	public static EnumFacing getDirection(World world, BlockPos pos)
	{
		return world.getBlockState(pos).getValue(StateList.directions);
	}
}
