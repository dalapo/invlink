package dalapo.invlink.helper;

import java.util.Collection;

import org.apache.logging.log4j.Level;

import dalapo.invlink.InventoryLink;

public class Logger
{
	private static void log(Level level, Object object)
	{
		InventoryLink.logger.log(level, object);
	}
	
	public static void info(Object obj)
	{
		log(Level.INFO, obj);
	}
	
	public static void debug(Object obj)
	{
		log(Level.DEBUG, obj);
	}
	
	public static void warn(Object obj)
	{
		log(Level.WARN, obj);
	}
	
	public static void error(Object obj)
	{
		log(Level.ERROR, obj);
	}
	
	public static void infoList(Collection<?> c)
	{
		c.forEach(obj -> info(obj));
	}
}