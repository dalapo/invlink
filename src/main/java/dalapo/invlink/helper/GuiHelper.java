package dalapo.invlink.helper;

import net.minecraftforge.fml.relauncher.SideOnly;
import dalapo.invlink.reference.NameList;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.relauncher.Side;

@SideOnly(Side.CLIENT)
public class GuiHelper
{
	private GuiHelper() {}
	
	public static ResourceLocation formatTexLocation(String texName)
	{
		return new ResourceLocation(NameList.MOD_ID + ":textures/gui/" + texName + ".png");
	}
	
	public static void bindTexture(String tex)
	{
		Minecraft.getMinecraft().getTextureManager().bindTexture(formatTexLocation(tex));
	}
	
	public static void renderTooltip(GuiScreen parent, int x, int y, String tooltip)
	{
		parent.drawHoveringText(tooltip, x, y);
	}
}