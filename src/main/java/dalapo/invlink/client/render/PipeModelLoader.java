package dalapo.invlink.client.render;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import dalapo.invlink.helper.Logger;
import dalapo.invlink.reference.NameList;
import net.minecraft.client.resources.IResourceManager;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.model.ICustomModelLoader;
import net.minecraftforge.client.model.IModel;

public class PipeModelLoader implements ICustomModelLoader
{
//	public static final PipeModel PIPE_MODEL = new PipeModel("model_pipe");
//	public static final PipeModel DET_OFF_MODEL = new PipeModel("model_det_off");
//	public static final PipeModel DET_ON_MODEL = new PipeModel("model_det_on");
	
	private static Map<String, PipeModel> modelMap = new HashMap<>();
	
	static {
		modelMap.put("pipe", new PipeModel("pipe"));
		modelMap.put("det_off", new PipeModel("det_off"));
		modelMap.put("det_on", new PipeModel("det_on"));
	}

	@Override
	public void onResourceManagerReload(IResourceManager resourceManager) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean accepts(ResourceLocation modelLocation)
	{
		return modelLocation.getResourceDomain().equals(NameList.MOD_ID) && modelMap.containsKey(modelLocation.getResourcePath());
	}

	@Override
	public IModel loadModel(ResourceLocation modelLocation) throws Exception
	{
		return modelMap.get(modelLocation.getResourcePath());
	}
}