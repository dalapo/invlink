package dalapo.invlink.client.render;

import java.util.Collection;
import java.util.Collections;
import java.util.function.Function;

import com.google.common.collect.ImmutableSet;

import dalapo.invlink.reference.NameList;
import net.minecraft.client.renderer.block.model.IBakedModel;
import net.minecraft.client.renderer.texture.TextureAtlasSprite;
import net.minecraft.client.renderer.vertex.VertexFormat;
import net.minecraft.util.BlockRenderLayer;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.ForgeHooksClient;
import net.minecraftforge.client.model.IModel;
import net.minecraftforge.common.model.IModelState;
import net.minecraftforge.common.model.TRSRTransformation;

public class PipeModel implements IModel
{
	public final String name;
	public PipeModel(String name)
	{
		this.name = name;
	}
	
	@Override
	public IBakedModel bake(IModelState state, VertexFormat format, Function<ResourceLocation, TextureAtlasSprite> bakedTextureGetter)
	{
		ForgeHooksClient.setRenderLayer(BlockRenderLayer.TRANSLUCENT);
		return new PipeBakedModel(state, format, bakedTextureGetter, name);
	}
	
	@Override
    public Collection<ResourceLocation> getDependencies()
	{
        return Collections.emptySet();
    }

    @Override
    public Collection<ResourceLocation> getTextures()
    {
        return ImmutableSet.of(new ResourceLocation(NameList.MOD_ID, "blocks/" + name));
    }

    @Override
    public IModelState getDefaultState()
    {
        return TRSRTransformation.identity();
    }
}