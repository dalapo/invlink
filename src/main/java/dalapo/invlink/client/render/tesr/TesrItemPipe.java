package dalapo.invlink.client.render.tesr;

import org.lwjgl.opengl.GL11;

import dalapo.invlink.helper.Logger;
import dalapo.invlink.helper.RenderHelper;
import dalapo.invlink.helper.RenderHelper.Point;
import dalapo.invlink.network.RoutedItem;
import dalapo.invlink.network.RoutedItemClient;
import dalapo.invlink.tileentity.TileEntityItemPipe;
import net.minecraft.client.renderer.BufferBuilder;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.tileentity.TileEntitySpecialRenderer;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.text.ITextComponent;
import net.minecraftforge.fml.relauncher.SideOnly;
import net.minecraftforge.fml.relauncher.Side;


@SideOnly(Side.CLIENT)
public class TesrItemPipe extends TileEntitySpecialRenderer<TileEntityItemPipe>
{
	public void render(TileEntityItemPipe te, double x, double y, double z, float partialTicks, int destroyStage, float alpha)
    {
		super.render(te, x, y, z, partialTicks, destroyStage, alpha);
		
//		if (te.getColour() != EnumColour.NONE)
//		{
//			GlStateManager.pushMatrix();
//			GlStateManager.translate(x+0.25, y+0.25, z+0.25);
//			GlStateManager.glLineWidth(2.0F);
//			GlStateManager.disableBlend();
//			GlStateManager.disableLighting();
//			GlStateManager.disableTexture2D();
//			GlStateManager.shadeModel(GL11.GL_FLAT);
//			GlStateManager.enableAlpha();
//			
//			EnumColour colour = te.getColour();
//			GlStateManager.color(colour.red() / 255F, colour.green() / 255F, colour.blue() / 255F);
//			Tessellator v5 = Tessellator.getInstance();
//			BufferBuilder buffer = v5.getBuffer();
//			buffer.begin(GL11.GL_LINES, DefaultVertexFormats.POSITION);
//			Point[] points = new Point[8];
//			for (int i=0; i<8; i++)
//			{
//				points[i] = new Point(i & 1, (i >> 1) & 1, (i >> 2) & 1);
//				points[i].scale(0.5, 0.5, 0.5);
//			}
//			
//			for (int i=0; i<8; i++)
//			{
//				for (int j=0; j<i; j++)
//				{
//					boolean doDraw = false;
//					for (int d=0; d<3; d++)
//					{
//						if (points[i].getField(d) == points[j].getField(d)) doDraw = true;
//					}
//					if (doDraw) RenderHelper.drawLine(buffer, points[i], points[j]);
//				}
//			}
//			v5.draw();
//			GlStateManager.enableTexture2D();
//			GlStateManager.enableLighting();
//			GlStateManager.disableAlpha();
//			GlStateManager.enableBlend();
//			GlStateManager.color(1F, 1F, 1F, 1F);
//			GlStateManager.shadeModel(GL11.GL_SMOOTH);
//			GlStateManager.popMatrix();
//		}
		
		GlStateManager.pushMatrix();
		GlStateManager.translate(x+0.5, y+0.5, z+0.5);
		GlStateManager.scale(0.4, 0.4, 0.4);
		for (RoutedItemClient ri : te.getClientItems())
		{
			GlStateManager.pushMatrix();
			double d = 1.0 / RoutedItem.TICKS;
			EnumFacing dir = ri.age < 0 ? ri.entryDir : ri.exitDir;
			if (dir != null) GlStateManager.translate(dir.getFrontOffsetX()*ri.age*d, dir.getFrontOffsetY()*ri.age*d, dir.getFrontOffsetZ()*ri.age*d);
			RenderHelper.renderItemStack(ri.is);
			GlStateManager.popMatrix();
		}
		GlStateManager.popMatrix();
//        ITextComponent itextcomponent = te.getDisplayName();
//
//        if (itextcomponent != null && this.rendererDispatcher.cameraHitResult != null && te.getPos().equals(this.rendererDispatcher.cameraHitResult.getBlockPos()))
//        {
//            this.setLightmapDisabled(true);
//            this.drawNameplate(te, itextcomponent.getFormattedText(), x, y, z, 12);
//            this.setLightmapDisabled(false);
//        }
    }
}