package dalapo.invlink.client.gui;

import dalapo.invlink.auxiliary.IInventoriedTile;
import dalapo.invlink.tileentity.TileEntityBase;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.Slot;
import net.minecraftforge.items.SlotItemHandler;

public class ContainerInventory extends ContainerBase
{
	public ContainerInventory(int rows, int cols, IInventory player, IInventoriedTile tile)
	{
		super((TileEntityBase)tile);
		int slot = 0;
		for (int row=0; row<rows; row++)
		{
			for (int col=0; col<cols; col++, slot++)
			{
				this.addSlotToContainer(new SlotItemHandler(tile.getInventory(), slot, (89 - (9 * cols)) + (col * 18), (53 - (18 * rows)) + (row * 18)));
			}
		}
		
		if (player != null)
		{
			for (int y = 0; y < 3; ++y)
			{
		        for (int x = 0; x < 9; ++x, slot++)
		        {
		            this.addSlotToContainer(new Slot(player, x + y * 9 + 9, 8 + x * 18, 84 + y * 18));
		        }
		    }
			
			// Player hotbar
			for (int i=0; i<9; i++)
			{
				this.addSlotToContainer(new Slot(player, i, 8 + i * 18, 142));
			}
		}
	}
}