package dalapo.invlink.client.gui;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.ClickType;
import net.minecraft.item.ItemStack;
import net.minecraftforge.items.IItemHandler;
import net.minecraftforge.items.SlotItemHandler;

public abstract class SlotCustom extends SlotItemHandler
{
	protected IItemHandler handler;
	protected ContainerBase parent;
	
	public SlotCustom(IItemHandler itemHandler, int index, int xPosition, int yPosition, ContainerBase parent)
	{
		super(itemHandler, index, xPosition, yPosition);
		this.parent = parent;
		this.handler = itemHandler;
	}
	
	public ItemStack insert(ItemStack is, boolean simulate)
	{
		return handler.insertItem(slotNumber, is, simulate);
	}
	
	public ItemStack clear()
	{
		return handler.extractItem(slotNumber, handler.getStackInSlot(slotNumber).getCount(), false);
	}
	
	public abstract ItemStack handleClick(int dragType, ClickType clickTypeIn, EntityPlayer player, ItemStack holding);
}