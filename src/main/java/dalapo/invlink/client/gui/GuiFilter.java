package dalapo.invlink.client.gui;

import dalapo.invlink.tileentity.TileEntityBase;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.IInventory;

public class GuiFilter extends GuiBasicContainer
{

	public GuiFilter(ContainerBase inventorySlotsIn, int xSize, int ySize, IInventory playerInv, TileEntityBase te)
	{
		super("filter", inventorySlotsIn, xSize, ySize, playerInv, te);
	}

	@Override
	protected void drawGuiContainerBackgroundLayer(float partialTicks, int mouseX, int mouseY)
	{
		super.drawGuiContainerBackgroundLayer(partialTicks, mouseX, mouseY);
	}
}