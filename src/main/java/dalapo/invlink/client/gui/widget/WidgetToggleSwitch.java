package dalapo.invlink.client.gui.widget;

import dalapo.invlink.client.gui.GuiTileEntity;
import dalapo.invlink.helper.GuiHelper;
import dalapo.invlink.packet.PacketHandler;
import dalapo.invlink.packet.PacketToggleField;
import net.minecraft.client.renderer.GlStateManager;

public class WidgetToggleSwitch extends InvLinkWidget
{
	private int id;
//	private boolean state;
	private String offMessage;
	private String onMessage;
	
	public WidgetToggleSwitch(GuiTileEntity parent, int id, int x, int y, String off, String on)
	{
		super(parent, x, y, 18, 10);
		this.id = id;
		onMessage = on;
		offMessage = off;
	}

	public void init()
	{
//		setState(parent.getTile().getField(id) > 0);
	}
	
	@Override
	public void handle(int mouseX, int mouseY, int mouseButton, boolean shift)
	{
		getParent().getTile().toggleField(id);
		PacketHandler.sendToServer(new PacketToggleField(getParent().getTile().getPos(), id));
	}

	@Override
	public String getTooltip()
	{
		return getParent().getTile().getField(id) == 0 ? offMessage : onMessage;
	}

	@Override
	public void draw(int guiLeft, int guiTop)
	{
		GlStateManager.pushMatrix();
		GuiHelper.bindTexture("widgets");
		GlStateManager.color(1F, 1F, 1F, 1F);
		this.drawTexturedModalRect(x + guiLeft, y + guiTop, 0, getParent().getTile().getField(id) == 0 ? 10 : 0, width, height);
		GlStateManager.popMatrix();
	}
}