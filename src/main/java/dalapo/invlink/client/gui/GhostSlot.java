package dalapo.invlink.client.gui;

import dalapo.invlink.helper.Logger;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.ClickType;
import net.minecraft.item.ItemStack;
import net.minecraftforge.items.IItemHandler;

public class GhostSlot extends SlotCustom
{
	public GhostSlot(IItemHandler itemHandler, int index, int xPosition, int yPosition, ContainerBase parent)
	{
		super(itemHandler, index, xPosition, yPosition, parent);
	}

	@Override
	public ItemStack handleClick(int dragType, ClickType clickTypeIn, EntityPlayer player, ItemStack holding)
	{
		Logger.info(String.format("%s, %s", clickTypeIn, dragType));
		if (dragType == 0) // Left click
		{
			if (clickTypeIn == ClickType.PICKUP)
			{
				if (!holding.isItemEqual(getStack())) clear();
				insert(holding.copy(), false);
			}
		}
		else if (dragType == 1) // Right click
		{
			if (getStack().isEmpty())
			{
				ItemStack toInsert = holding.copy();
				toInsert.setCount(1);
				insert(toInsert, false);
			}
			else if (clickTypeIn == ClickType.PICKUP)
			{
				getStack().grow(1);
			}
			else if (clickTypeIn == ClickType.QUICK_MOVE)
			{
				getStack().shrink(1);
			}
		}
		return holding; // No matter what, return holding
	}
}