package dalapo.invlink.client.gui;

import dalapo.invlink.auxiliary.IGuiTile;
import dalapo.invlink.tileentity.TileEntityBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.network.IGuiHandler;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class InvLinkGuiHandler implements IGuiHandler
{
	@Override
	public Object getServerGuiElement(int ID, EntityPlayer player, World world, int x, int y, int z)
	{
		IGuiTile te = (IGuiTile)world.getTileEntity(new BlockPos(x, y, z));
		return te.getContainer(player);
	}

	@Override
	@SideOnly(Side.CLIENT)
	public Object getClientGuiElement(int ID, EntityPlayer player, World world, int x, int y, int z)
	{
		IGuiTile te = (IGuiTile)world.getTileEntity(new BlockPos(x, y, z));
		return te.getGui(player);
	}
}