package dalapo.invlink.client.gui;

import dalapo.invlink.tileentity.TileEntityBase;
import net.minecraft.tileentity.TileEntity;

public interface GuiTileEntity
{
	public TileEntityBase getTile();
}
