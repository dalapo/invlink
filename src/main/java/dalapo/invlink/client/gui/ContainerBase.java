package dalapo.invlink.client.gui;

import dalapo.invlink.helper.InvLinkMathHelper;
import dalapo.invlink.helper.Logger;
import dalapo.invlink.tileentity.TileEntityBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.ClickType;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.IContainerListener;
import net.minecraft.item.ItemStack;

public class ContainerBase extends Container
{
	private TileEntityBase tile;
	private int[] data;
	
	public ContainerBase(TileEntityBase te)
	{
		tile = te;
	}
	
	@Override
	public boolean canInteractWith(EntityPlayer playerIn)
	{
		return true;
	}
	
	@Override
	public ItemStack slotClick(int slotId, int dragType, ClickType clickTypeIn, EntityPlayer player)
	{
		if (InvLinkMathHelper.isInRange(slotId, 0, inventorySlots.size()) && inventorySlots.get(slotId) instanceof SlotCustom)
		{
			return ((SlotCustom)inventorySlots.get(slotId)).handleClick(dragType, clickTypeIn, player, player.inventory.getItemStack());
		}
		else
		{
			return super.slotClick(slotId, dragType, clickTypeIn, player);
		}
	}
	
	public TileEntityBase getTile()
	{
		return tile;
	}
	
	public int getData(int id)
	{
		if (id >= 0 && id < data.length) return data[id];
		return 0;
	}
	
	public void setData(int id, int val)
	{
		if (id >= 0 && id <= data.length)
		{
			data[id] = val;
			detectAndSendChanges();
		}
	}
	
	public void toggleData(int id)
	{
		if (id >= 0 && id <= data.length)
		{
			if (getData(id) == 0) setData(id, 1);
			else setData(id, 0);
		}
	}
	
	@Override
	public void detectAndSendChanges()
	{
		super.detectAndSendChanges();
		boolean changeEverything = false;
		boolean[] hasChangedField = new boolean[tile.getFieldCount()];
		if (data == null)
		{
			changeEverything = true;
			data = new int[tile.getFieldCount()];
		}
		
		for (int i=0; i<tile.getFieldCount(); i++)
		{
			if (changeEverything || tile.getField(i) != data[i])
			{
				data[i] = tile.getField(i);
				hasChangedField[i] = true;
			}
		}
		
		for (IContainerListener listener : this.listeners)
		{
			for (int i=0; i<data.length; i++)
			{
				if (hasChangedField[i])
				{
					listener.sendWindowProperty(this, i, getTile().getField(i));
				}
			}
		}
	}
}