package dalapo.invlink.client.gui;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import dalapo.invlink.client.gui.widget.InvLinkWidget;
import dalapo.invlink.helper.GuiHelper;
import dalapo.invlink.reference.NameList;
import dalapo.invlink.tileentity.TileEntityBase;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.IInventory;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ResourceLocation;

public class GuiBasicContainer extends GuiContainer implements GuiTileEntity
{
	protected IInventory playerInv;
	protected TileEntityBase tile;
	protected String texture;
	private List<InvLinkWidget> widgets = new ArrayList<>();
	
	public GuiBasicContainer(String texName, ContainerBase inventorySlotsIn, int xSize, int ySize, IInventory playerInv, TileEntityBase te)
	{
		super(inventorySlotsIn);
		this.playerInv = playerInv;
		this.tile = te;
		this.xSize = xSize;
		this.ySize = ySize;
		this.texture = texName;
	}
	
	@Override
	public void initGui()
	{
		super.initGui();
		widgets.forEach(widget -> widget.init());
	}

	public GuiBasicContainer addWidget(InvLinkWidget widget)
	{
		widgets.add(widget);
		return this;
	}
	
	@Override
	public void drawScreen(int mouseX, int mouseY, float partialTicks)
	{
		this.drawDefaultBackground();
		super.drawScreen(mouseX, mouseY, partialTicks);
		this.renderHoveredToolTip(mouseX, mouseY);
	}
	
	@Override
	protected void drawGuiContainerBackgroundLayer(float partialTicks, int mouseX, int mouseY)
	{
		GuiHelper.bindTexture(texture);
		drawTexturedModalRect(guiLeft, guiTop, 0, 0, xSize, ySize);
		widgets.forEach(widget -> widget.draw(guiLeft, guiTop));
	}
	
	@Override
	protected void drawGuiContainerForegroundLayer(int mouseX, int mouseY)
	{
		super.drawGuiContainerForegroundLayer(mouseX, mouseY);
	}

	@Override
	public void mouseClicked(int mouseX, int mouseY, int mouseButton) throws IOException
	{
		super.mouseClicked(mouseX, mouseY, mouseButton);
		for (InvLinkWidget widget : widgets)
		{
			if (widget.isPointInBounds(mouseX - guiLeft, mouseY - guiTop))
			{
				widget.handle(mouseX - guiLeft, mouseY - guiTop, mouseButton, isShiftKeyDown());
			}
		}
	}
	
	@Override
	public void renderHoveredToolTip(int mouseX, int mouseY)
	{
		super.renderHoveredToolTip(mouseX, mouseY);
		int mmX = mouseX - guiLeft;
		int mmY = mouseY - guiTop;
		for (InvLinkWidget widget : widgets)
		{
			if (widget.isPointInBounds(mmX, mmY)) GuiHelper.renderTooltip(this, mouseX, mouseY, widget.getTooltip());
		}
	}
	
	@Override
	public TileEntityBase getTile()
	{
		return tile;
	}
}