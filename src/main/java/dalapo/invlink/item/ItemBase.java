package dalapo.invlink.item;

import dalapo.invlink.init.ItemRegistry;
import dalapo.invlink.reference.NameList;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.NonNullList;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class ItemBase extends Item
{
	public final String name;
	private int numSubtypes = 1;
	
	public ItemBase(String name)
	{
		this.name = name;
		setUnlocalizedName(NameList.MOD_ID + ":" + name);
		setRegistryName(name);
		ItemRegistry.addItem(this);
	}
	
	protected void setSubtypes(int num)
	{
		if (num > 0)
		{
			hasSubtypes = true;
			numSubtypes = num;
		}
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public void getSubItems(CreativeTabs tab, NonNullList<ItemStack> items)
	{
		if (hasSubtypes)
		{
			if (tab.equals(getCreativeTab()))
			{
				for (int i=0; i<numSubtypes; i++)
				{
					items.add(new ItemStack(this, 1, i));
				}
			}
		}
		else super.getSubItems(tab, items);
	}
	
	@SideOnly(Side.CLIENT)
	public void initModel()
	{
		if (hasSubtypes)
		{
			for (int i=0; i<numSubtypes; i++)
			{
				ModelLoader.setCustomModelResourceLocation(this, i, new ModelResourceLocation(getRegistryName() + "_" + i, "inventory"));
			}
		}
		ModelLoader.setCustomModelResourceLocation(this, 0, new ModelResourceLocation(getRegistryName(), "inventory"));
	}
}