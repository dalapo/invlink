package dalapo.invlink.item;

import dalapo.invlink.auxiliary.IUpgradeable;
import dalapo.invlink.packet.PacketHandler;
import dalapo.invlink.packet.PacketUpgradeTile;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumActionResult;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class ItemUpgrade extends ItemBase
{
	public ItemUpgrade(String name)
	{
		super(name);
		setCreativeTab(CreativeTabs.TOOLS);
		setSubtypes(1);
	}
	
	@Override
	public EnumActionResult onItemUse(EntityPlayer ep, World world, BlockPos pos, EnumHand hand, EnumFacing facing, float hitX, float hitY, float hitZ)
	{
		TileEntity te = world.getTileEntity(pos);
		if (te instanceof IUpgradeable)
		{
			int id = ep.getHeldItem(hand).getItemDamage();
			boolean success = ((IUpgradeable)te).applyUpgrade(id);
			if (success)
			{
				PacketHandler.sendToAll(new PacketUpgradeTile(pos, id));
				if (!ep.isCreative()) ep.getHeldItem(hand).shrink(1);
				return EnumActionResult.SUCCESS;
			}
			else return EnumActionResult.FAIL;
		}
		return EnumActionResult.PASS;
	}
}