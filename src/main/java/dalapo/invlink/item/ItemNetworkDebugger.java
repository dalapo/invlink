package dalapo.invlink.item;

import dalapo.invlink.auxiliary.Pair;
import dalapo.invlink.helper.ChatHelper;
import dalapo.invlink.helper.Logger;
import dalapo.invlink.init.ItemRegistry;
import dalapo.invlink.network.PipeNode;
import dalapo.invlink.packet.PacketHandler;
import dalapo.invlink.packet.PacketSendChatInfo;
import dalapo.invlink.tileentity.TileEntityItemPipe;
import dalapo.invlink.tileentity.TileEntityTeleportPipe;
import net.minecraft.client.Minecraft;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.Item;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumActionResult;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class ItemNetworkDebugger extends ItemBase
{
	public ItemNetworkDebugger(String name)
	{
		super(name);
		setCreativeTab(CreativeTabs.TRANSPORTATION);
	}
	
	@Override
	public EnumActionResult onItemUse(EntityPlayer ep, World world, BlockPos pos, EnumHand hand, EnumFacing facing, float hitX, float hitY, float hitZ)
	{
		TileEntity te = world.getTileEntity(pos);
		if (te instanceof TileEntityItemPipe && !world.isRemote)
		{
			TileEntityItemPipe pipe = (TileEntityItemPipe)te;
			String info = "Network: " + pipe.getNetwork().id + "; ";
			if (pipe.isNode())
			{
				info += (pipe.getNetwork().findNode(pipe).toString());
			}
			else if (pipe.isEdge())
			{
				Pair<PipeNode, PipeNode> edge = pipe.getNetwork().getEdge(pipe);
				info += "EDGE: " + edge.toString();
			}
			else if (pipe.isDeadEnd())
			{
				info += "DEAD END: Connected to " + pipe.getNodes().a;
			}
			Logger.info(info);
//			PacketHandler.sendToPlayer(new PacketSendChatInfo(info), ep);
			return EnumActionResult.SUCCESS;
		}
		else if (te instanceof TileEntityTeleportPipe && !world.isRemote)
		{
			TileEntityTeleportPipe tele = (TileEntityTeleportPipe)te;
			if (tele.getLink() != null) Logger.info(String.format("TELEPORT linked to %s", tele.getLink().getPos()));
			else Logger.info("TELEPORT, unlinked");
		}
		return EnumActionResult.PASS;
	}
}