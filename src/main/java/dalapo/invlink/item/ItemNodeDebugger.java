package dalapo.invlink.item;

import java.util.Set;

import dalapo.invlink.helper.Logger;
import dalapo.invlink.network.NetworkedInventory;
import dalapo.invlink.network.PipeNode;
import dalapo.invlink.tileentity.TileEntityItemPipe;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumActionResult;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class ItemNodeDebugger extends ItemBase
{
	public ItemNodeDebugger(String name)
	{
		super(name);
		setCreativeTab(CreativeTabs.TRANSPORTATION);
	}
	
	@Override
	public EnumActionResult onItemUse(EntityPlayer ep, World world, BlockPos pos, EnumHand hand, EnumFacing facing, float hitX, float hitY, float hitZ)
	{
		TileEntity te = world.getTileEntity(pos);
		if (te instanceof TileEntityItemPipe && !world.isRemote)
		{
			TileEntityItemPipe pipe = (TileEntityItemPipe)te;
			if (pipe.isNode())
			{
				Logger.info("Network: " + pipe.getNetwork().id + "\n");
				PipeNode p = pipe.getNodes().a;
				if (ep.isSneaking()) p.calcConnections();
				Logger.info(p.toString() + "\n");
				Logger.info("NEIGHBOURS:");
				p.getNeighbours().forEach(neighbour -> Logger.info(String.format("%s, distance %s, immediately connected to %s", neighbour, p.getImmediateDistance(neighbour), p.getImmediateConnection(neighbour).getPos())));
				Logger.info("INVENTORIES:");
				Set<NetworkedInventory> inventories = p.getNetwork().getConnectedInventories(p);
				inventories.forEach(inv -> Logger.info(inv.toString()));
			}
			return EnumActionResult.SUCCESS;
		}
		return EnumActionResult.PASS;
	}
}