package dalapo.invlink.item;

import java.util.List;

import dalapo.invlink.auxiliary.ISpecialItem;
import dalapo.invlink.block.BlockBase;
import net.minecraft.block.Block;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class ItemBlockSubtypes extends ItemBlock
{

	public ItemBlockSubtypes(BlockBase block)
	{
		super(block);
		if (!(block instanceof ISpecialItem)) throw new IllegalArgumentException();
		this.setMaxDamage(0);
		this.setHasSubtypes(true);
	}
	
	public int getMetadata(int dmg)
	{
		return dmg;
	}
	
	@Override
	public String getUnlocalizedName(ItemStack stack)
	{
		return ((ISpecialItem)block).getName(stack) + ":" + stack.getItemDamage();
	}
}