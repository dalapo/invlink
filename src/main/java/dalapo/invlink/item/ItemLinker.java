package dalapo.invlink.item;

import dalapo.invlink.InvLinkConfigManager;
import dalapo.invlink.auxiliary.ILinkable;
import dalapo.invlink.helper.ChatHelper;
import dalapo.invlink.helper.InvLinkMathHelper;
import dalapo.invlink.helper.Logger;
import dalapo.invlink.tileentity.TileEntityTeleportPipe;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumActionResult;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class ItemLinker extends ItemBase
{
	public ItemLinker(String name)
	{
		super(name);
		setCreativeTab(CreativeTabs.TRANSPORTATION);
		setMaxStackSize(1);
	}
	
	@Override
	public EnumActionResult onItemUseFirst(EntityPlayer ep, World world, BlockPos pos, EnumFacing side, float hitX, float hitY, float hitZ, EnumHand hand)
	{
		TileEntity te = world.getTileEntity(pos);
		if (te instanceof ILinkable)
		{
			ItemStack is = ep.getHeldItem(hand);
			if (!is.hasTagCompound())
			{
				ChatHelper.sendChatToPlayer(ep, String.format("Stored position (%s, %s, %s)", pos.getX(), pos.getY(), pos.getZ()));
				NBTTagCompound tag = new NBTTagCompound();
				tag.setLong("pos", pos.toLong());
				is.setTagCompound(tag);
			}
			else
			{
				BlockPos storedPos = BlockPos.fromLong(is.getTagCompound().getLong("pos"));
				if (((ILinkable)te).link(storedPos, ep)) is.setTagCompound(null);
			}
			return EnumActionResult.SUCCESS;
		}
		return EnumActionResult.PASS;
	}
}
