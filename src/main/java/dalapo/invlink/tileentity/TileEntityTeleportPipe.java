package dalapo.invlink.tileentity;

import dalapo.invlink.InvLinkConfigManager;
import dalapo.invlink.auxiliary.ILinkable;
import dalapo.invlink.auxiliary.Pair;
import dalapo.invlink.helper.BlockHelper;
import dalapo.invlink.helper.ChatHelper;
import dalapo.invlink.helper.InvLinkMathHelper;
import dalapo.invlink.helper.Logger;
import dalapo.invlink.network.INetworkedObject;
import dalapo.invlink.network.PipeNetwork;
import dalapo.invlink.network.PipeNode;
import dalapo.invlink.packet.PacketHandler;
import dalapo.invlink.reference.StateList;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.BlockPos;

// Modify TileEntityItemPipe.getConnectionDirections() as follows:
// If it is connected to a teleport pipe, connect it to the corresponding pipe on the other side.
public class TileEntityTeleportPipe extends TileEntityBase implements INetworkedObject, ILinkable
{
	BlockPos link;
	TileEntityItemPipe pipe;
	
	public TileEntityTeleportPipe getLink()
	{
		if (link == null) return null;
		return (TileEntityTeleportPipe) world.getTileEntity(link);
	}
	
	public TileEntityItemPipe getPipe()
	{
		return pipe;
	}
	
	public boolean checkPipe()
	{
		TileEntity te = world.getTileEntity(pos.offset(world.getBlockState(pos).getValue(StateList.directions)));
		if (te instanceof TileEntityItemPipe)
		{
			pipe = (TileEntityItemPipe)te;
			return true;
		}
		return false;
	}
	
	public void onLoad()
	{
		checkPipe();
	}
	
//	private void linkPipe(TileEntityTeleportPipe other, boolean reciprocate)
//	{
//		unlink();
//		other.unlink();
//		link = other.pos;
//		
//		if (checkPipe())
//		{
//			pipe.recalcConnections();
//			pipe.getNodes().a.calcConnections();
//			if (!reciprocate)
//			{
//				pipe.neighbourAdded(world.getBlockState(pos).getValue(StateList.directions).getOpposite(), other.pipe);
//				other.pipe.neighbourAdded(world.getBlockState(link).getValue(StateList.directions).getOpposite(), pipe);
//			}
//		}
//		if (reciprocate) other.linkPipe(this, false);
//		markDirty();
////		Logger.info(String.format("Linked pipe %s to pipe %s", pos, other.pos));
//	}
	
	private void unlink()
	{
		if (pipe != null) pipe.neighbourRemoved(InvLinkMathHelper.getRelativeDirection(pipe.getPos(), pos));
		link = null;
	}
	
	@Override
	public void remove()
	{
		((TileEntityTeleportPipe)world.getTileEntity(link)).unlink();
		unlink();
	}
	
	public void linkPipe(TileEntityTeleportPipe other)
	{
		if (!world.isRemote)
		{
			unlink();
			link = other.pos;
			other.unlink();
			other.link = pos;
			
			if (checkPipe() && other.checkPipe())
			{
				pipe.recalcConnections();
				if (pipe.getNodes().a != null) pipe.getNodes().a.calcConnections();
				other.pipe.recalcConnections();
				if (other.pipe.getNodes().a != null) other.pipe.getNodes().a.calcConnections();
				pipe.neighbourAdded(BlockHelper.getDirection(world, pos).getOpposite(), other.pipe);
				other.pipe.neighbourAdded(BlockHelper.getDirection(world, other.pos).getOpposite(), pipe);
			}
		}
	}

	@Override
	public PipeNetwork getNetwork()
	{
		// TODO Auto-generated method stub
		return pipe.getNetwork();
	}

	@Override
	public void setNetwork(PipeNetwork net)
	{
		// no-op
	}

	@Override
	public boolean canConnectToSide(EnumFacing side)
	{
		return (side == world.getBlockState(pos).getValue(StateList.directions));
	}
	
	@Override
	public boolean link(BlockPos other, EntityPlayer ep)
	{
		TileEntity te = world.getTileEntity(other);
		if (te instanceof TileEntityTeleportPipe && !pos.equals(other) && InvLinkMathHelper.getAbsoluteDistance(pos, other) <= InvLinkConfigManager.maxTeleporterRange)
		{
			if (!world.isRemote) linkPipe((TileEntityTeleportPipe)te);
			else ChatHelper.sendChatToPlayer(ep, "Successfully linked teleporters!");
			return true;
		}
		else if (InvLinkMathHelper.getAbsoluteDistance(pos, other) >= InvLinkConfigManager.maxTeleporterRange)
		{
			ChatHelper.sendChatToPlayer(ep, "Out of range!");
		}
		else if (pos.equals(other))
		{
			ChatHelper.sendChatToPlayer(ep, "Can't link a teleporter to itself.");
		}
		return false;
	}
	
	@Override
	public NBTTagCompound writeToNBT(NBTTagCompound nbt)
	{
		super.writeToNBT(nbt);
		if (link != null) nbt.setLong("link", link.toLong());
		return nbt;
	}
	
	@Override
	public void readFromNBT(NBTTagCompound nbt)
	{
		super.readFromNBT(nbt);
		if (nbt.hasKey("link")) link = BlockPos.fromLong(nbt.getLong("link"));
	}
}