package dalapo.invlink.tileentity;

import java.util.LinkedList;

import dalapo.invlink.auxiliary.Pair;
import dalapo.invlink.helper.InventoryHelper;
import dalapo.invlink.helper.Logger;
import dalapo.invlink.network.PipeNode;
import dalapo.invlink.network.RoutedItem;
import dalapo.invlink.packet.PacketCreateRoutedItem;
import dalapo.invlink.packet.PacketHandler;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraftforge.items.IItemHandler;

public class TileEntityNotRetriever extends TileEntityNetworkInserter
{
	@Override
	public void onRedstoneEdge()
	{
		TileEntity te = world.getTileEntity(pos.offset(direction.getOpposite()));
		if (te instanceof TileEntityItemPipe && !world.isRemote)
		{
			TileEntityItemPipe pipe = (TileEntityItemPipe)te;
//			Logger.info(pipe.getNodes());
			Pair<PipeNode, EnumFacing> pair = pipe.getNetwork().findSuitableSource(pipe.getNodes().a, filter);
			if (pair != null)
			{
				IItemHandler inv = pair.a.getInventory(pair.b).inventory;
				ItemStack is = InventoryHelper.extractFirstItem(inv, filter);
				PipeNode node = pipe.getNodes().a;
				RoutedItem item = new RoutedItem(pair.b.getOpposite(), new Pair<>(node, direction), is);
				LinkedList<PipeNode> path = pipe.getNetwork().pathNodal(pair.a, pipe.getNodes().a);
				item.setPath(path);
				pair.a.getTile().addItem(item);
				PacketHandler.sendToAll(new PacketCreateRoutedItem(item));
			}
		}
	}
}