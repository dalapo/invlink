package dalapo.invlink.tileentity;

import dalapo.invlink.helper.BlockHelper;
import dalapo.invlink.network.RoutedItem;
import dalapo.invlink.reference.StateList;

public class TileEntityDetectorPipe extends TileEntityItemPipe
{
	@Override
	public void update()
	{
		super.update();
	}
	
	@Override
	public void addItem(RoutedItem item)
	{
		super.addItem(item);
		world.setBlockState(pos, world.getBlockState(pos).withProperty(StateList.pipetypes, StateList.PipeType.DETECTOR_ON));
		BlockHelper.updateBlock(world, pos);
		markDirty();
	}
	
	@Override
	public void onRemoval()
	{
		if (numItems() == 0)
		{
			world.setBlockState(pos, world.getBlockState(pos).withProperty(StateList.pipetypes, StateList.PipeType.DETECTOR_OFF));
			BlockHelper.updateBlock(world, pos);
			markDirty();
		}
	}
}