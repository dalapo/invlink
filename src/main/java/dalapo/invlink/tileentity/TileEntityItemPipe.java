package dalapo.invlink.tileentity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;
import com.google.common.collect.ImmutableSet;

import dalapo.invlink.auxiliary.Pair;
import dalapo.invlink.helper.BlockHelper;
import dalapo.invlink.helper.InvLinkMathHelper;
import dalapo.invlink.helper.Logger;
import dalapo.invlink.network.INetworkedObject;
import dalapo.invlink.network.PipeNetwork;
import dalapo.invlink.network.PipeNode;
import dalapo.invlink.network.RoutedItem;
import dalapo.invlink.network.RoutedItemClient;
import dalapo.invlink.packet.PacketCreateRoutedItem;
import dalapo.invlink.packet.PacketHandler;

import java.util.Set;

import net.minecraft.client.Minecraft;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.play.server.SPacketUpdateTileEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ITickable;
import net.minecraft.util.math.BlockPos;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import net.minecraftforge.items.CapabilityItemHandler;
import net.minecraftforge.items.IItemHandler;

public class TileEntityItemPipe extends TileEntityBase implements INetworkedObject, ITickable
{
	private boolean hasLoaded = false;
	private PipeNetwork network;
//	private Set<EnumFacing> blockedDirections = new HashSet<>(); Eventually, not now.
	private Map<EnumFacing, TileEntityItemPipe> connections = new HashMap<>();
	private Set<EnumFacing> teleports = new HashSet<>();
	private Map<EnumFacing, IItemHandler> inventories = new HashMap<>();
	/**
	 * Both nodes the same: Pipe is that node<p>
	 * Two different nodes: Pipe is part of an edge between the two nodes<p>
	 * One is null: Pipe is a dead end connected to the non-null node<p>
	 * Both are null: Pipe belongs to a nodeless network<p>
	 */
	private Pair<PipeNode, PipeNode> nodes = new Pair<>();
	private int numConnections;
	private boolean isBuilt = false;
	
	private int age = 0;
	private Set<RoutedItem> items = new HashSet<>();
	private Set<RoutedItemClient> clientSideItems = new HashSet<>();
	
	public boolean hasNetwork()
	{
		return network != null;
	}
	
	public PipeNetwork getNetwork()
	{
		return network;
	}
	
	public void setNetwork(PipeNetwork network)
	{
		this.network = network;
	}
	
	/**
	 * Placeholder method, meant to include support for blocking adjacent pipes from connecting. No such method exists at this time.
	 * @param other
	 * @return
	 */
	public boolean canConnectTo(TileEntityItemPipe other)
	{
		return true;
	}
	
	// Recalculates the tiles immediately connected to this pipe.
	public void recalcConnections()
	{
		connections.clear();
		teleports.clear();
		inventories.clear();
		for (EnumFacing f : EnumFacing.VALUES)
		{
//			try {
			TileEntity te = world.getTileEntity(pos.offset(f));
			if (te instanceof TileEntityItemPipe)
			{
				TileEntityItemPipe pipe = (TileEntityItemPipe)te;
				// Only connect to uncoloured or same-coloured pipes
				if (canConnectTo(pipe)) connections.put(f, (TileEntityItemPipe)te);
			}
			else if (te instanceof TileEntityTeleportPipe && ((TileEntityTeleportPipe)te).canConnectToSide(f.getOpposite()))
			{
				TileEntityTeleportPipe tele = ((TileEntityTeleportPipe)te).getLink();
				if (tele != null)
				{
					tele.checkPipe();
					teleports.add(f);
					if (tele.getPipe() != null)
					{
						Logger.info("Adding teleport connection to " + tele.getPipe().getPos());
						connections.put(f, tele.getPipe());
					}
				}
			}
			else if (te != null && te.hasCapability(CapabilityItemHandler.ITEM_HANDLER_CAPABILITY, f.getOpposite()))
			{
				inventories.put(f, te.getCapability(CapabilityItemHandler.ITEM_HANDLER_CAPABILITY, f.getOpposite()));
			}
		}
	}
	
	public Set<EnumFacing> getConnectionDirections()
	{
		return connections.keySet();
	}
	
	// Meant for pipes in an edge (i.e. have exactly two connected pipes). When given a direction f that is one of the directions, returns the other one.
	public EnumFacing getOtherConnection(EnumFacing f)
	{
		if (!connections.keySet().contains(f)) return null; // enforce f actually being one of the connected directions
		// Super janky but whatever
		if (connections.size() == 1) return f; // turn around, every now and then I feel like I've hit a dead end
		else if (connections.size() == 2)
		{
			for (EnumFacing dir : connections.keySet())
			{
				if (f != dir) return dir;
			}
		}
		return null;
	}
	
	public Set<TileEntityItemPipe> getConnectedPipes()
	{
		Set<TileEntityItemPipe> s = new HashSet<>();
		for (EnumFacing f : connections.keySet())
		{
			s.add(connections.get(f));
		}
		return s;
	}
	
	public TileEntityItemPipe getConnectedPipe(EnumFacing dir)
	{
		return connections.get(dir);
	}
	
	// Tests to see if the pipe is registered as a node in the network. Not to be confused with shouldBeNode
	public boolean isNode()
	{
		return network != null && nodes.a == nodes.b && nodes.a != null;
	}
	
	public boolean hasTeleport(EnumFacing f)
	{
		return teleports.contains(f);
	}
	
	/**
	 * A pipe should be a node if it is connected to at least 3 other pipes (that is, an intersection) or is connected to an inventory.
	 * @return
	 */
	public boolean shouldBeNode()
	{
		return !inventories.isEmpty() || connections.size() >= 3;
	}
	
	public boolean isEdge()
	{
		return nodes.a != null && nodes.b != null && nodes.a != nodes.b; // or: nodes.a != nodes.b && nodes.a != null && nodes.b != null
	}
	
	public boolean isDeadEnd()
	{
		return nodes.a != null && nodes.b == null;
	}
	
	public void setNode(PipeNode node)
	{
//		network = node.getNetwork(); // this shouldn't be needed but apparently it is??
		// so why is it commented out, genius?
		nodes.a = node;
		nodes.b = node;
	}
	
	public void setEdge(PipeNode a, PipeNode b)
	{
		nodes.a = a;
		nodes.b = b;
	}
	
	public void setDeadEnd(PipeNode node)
	{
//		Thread.dumpStack(); // Literally, why did I have to do this?
		nodes.a = node;
		nodes.b = null;
	}
	
	public Map<EnumFacing, IItemHandler> getInventories()
	{
		return inventories;
	}
	
	// Only call findNode from setDead as predictable results are only guaranteed if the pipe is a dead end.
	private Pair<PipeNode, Set<TileEntityItemPipe>> findNode(Set<TileEntityItemPipe> visited, TileEntityItemPipe current)
	{
		visited.add(current);
		for (TileEntityItemPipe pipe : current.getConnectedPipes())
		{
			if (pipe.isNode())
			{
				return new Pair<>(pipe.nodes.a, visited);
			}
			else if (!visited.contains(pipe))
			{
				return findNode(visited, pipe);
			}
		}
		return new Pair<>(null, visited);
	}
	
	// we don't really care what this does because it'll be overwritten as soon as new nodes are made
	public void setDead()
	{
//		Logger.info("Calling setDead");
		if (isNode() || isEdge())
		{
			Pair<PipeNode, Set<TileEntityItemPipe>> pair = findNode(new HashSet<>(), this);
			pair.b.forEach(pipe -> {
				pipe.nodes.a = pair.a;
				pipe.nodes.b = null;
			});
		}
	}
	
	public Pair<PipeNode, PipeNode> getNodes()
	{
		return nodes;
	}
	
	private void buildNetworkHelper(TileEntityItemPipe pipe)
	{
		network.allPipes.add(pipe);
		pipe.setNetwork(network);
		pipe.recalcConnections();
		for (TileEntityItemPipe p : pipe.getConnectedPipes())
		{
			if (!network.allPipes.contains(p))
			{
				buildNetworkHelper(p);
			}
		}
	}
	
	private void buildNetwork()
	{
		network = new PipeNetwork();
		buildNetworkHelper(this);
		network.init();
	}
	
	// Loads and initializes the pipe.
	// Cannot use onLoad() due to chunkloading issues.
	public void load()
	{
		boolean foundNetwork = false;
		recalcConnections();
		
		if (!world.isRemote)
		{
			// Network should only exist server-side
			if (network == null)
			{
				for (EnumFacing f : getConnectionDirections()) // Check to see if neighbours already belong to a network, and join it if they do
				{
					TileEntityItemPipe pipe = getConnectedPipe(f);
					// With proper implementation, this will run only on newly placed pipes (never world load)
					if (foundNetwork && pipe.network != null && pipe.network != network) network.mergeNetwork(pipe.network);
					if (pipe.network != null)
					{
						pipe.connections.put(f.getOpposite(), this);
						pipe.network.addPipe(this);
						pipe.neighbourAdded(f.getOpposite(), this);
						foundNetwork = true; 
					}
				}
				if (!foundNetwork)
				{
					buildNetwork();
					// Pipe does not belong to a network, and neither do any of its neighbours. Create a new network.
				}
			}
		}
		else // Sorry, Linus
		{
			for (EnumFacing f : getConnectionDirections())
			{
				TileEntityItemPipe pipe = getConnectedPipe(f);
				pipe.connections.put(f.getOpposite(), this);
			}
		}
	}
	
	public void nonPipeNeighbourAdd(EnumFacing f, IItemHandler inv)
	{
		IItemHandler oldInv = inventories.get(f);
		recalcConnections();
		if (network != null)
		{
			if (!isNode())
			{
				network.addNewNode(this);
			}
			else if (inventories.get(f) != null && inventories.get(f) != oldInv)
			{
				network.addInventory(nodes.a, f, inv);
			}
		}
	}
	
	public void nonPipeNeighbourRemove(BlockPos neighbourPos)
	{
		recalcConnections();
		if (network != null)
		{
			if (isNode())
			{
				network.removeInventory(nodes.a, InvLinkMathHelper.getRelativeDirection(pos, neighbourPos));
				if (isNode() && !shouldBeNode())
				{
					network.destroyNode(this, false);
				}
			}
		}
	}
	
	public void neighbourAdded(EnumFacing dir, TileEntityItemPipe newPipe)
	{
//		Logger.info("NETWORK " + network.id + ": added neighbouring pipe to " + pos);
//		connections.put(dir, newPipe);
		if (network != null && shouldBeNode() && !isNode())
		{
			network.addNewNode(this);
		}
	}
	
	public void neighbourRemoved(EnumFacing dir)
	{
		if (isNode())
		{
			connections.remove(dir);
			if (!shouldBeNode()) network.nodeToEdge(this);
		}
		else if (isEdge())
		{
			connections.remove(dir);
			network.verifyEdge(nodes, true);
		}
		else if (isDeadEnd())
		{
			// do something, idk
		}
	}
	
	public void removeConnection(EnumFacing f)
	{
		connections.remove(f);
	}
	
	public void invalidate()
	{
		if (world.isRemote)
		{
			for (EnumFacing f : connections.keySet())
			{
				TileEntityItemPipe te = connections.get(f);
				te.connections.remove(f.getOpposite());
			}
		}
		super.invalidate();
	}
	
	// After removing a pipe, try to path from connected node A to connected node B. If possible, update edges. If not, split the network.
	// If the pipe itself is a node with n neighbours, the network may be split into n parts. Pick a random node as the master and try to
	// path it to other connecting nodes. Any that can still get there are fine; any that cannot are split into new networks. 
	public void remove()
	{
		if (!world.isRemote)
		{
			for (RoutedItem item : items)
			{
				EntityItem ei = new EntityItem(world, pos.getX()+0.5, pos.getY()+0.5, pos.getZ()+0.5, item.getItem());
				world.spawnEntity(ei);
			}
		}
		for (EnumFacing f : connections.keySet())
		{
			TileEntityItemPipe te = connections.get(f);
			te.connections.remove(f.getOpposite());
		}
		connections.clear();
		nodes.set(null, null);
		network.removeTile(this);
		for (EnumFacing f : EnumFacing.values())
		{
			TileEntity te = world.getTileEntity(pos.offset(f));
			// Notify neighbouring pipes that this one was removed
			if (te instanceof TileEntityItemPipe) ((TileEntityItemPipe)te).neighbourRemoved(f.getOpposite());
		}
	}
	
	public void addItem(RoutedItem item)
	{
		items.add(item);
		item.setPipe(this);
		item.updateDirections();
		markDirty();
		PacketHandler.sendToAll(new PacketCreateRoutedItem(item));
	}
	
	@Override
	public int hashCode()
	{
		return pos.hashCode() ^ world.hashCode(); // Is this safe???
	}
	
	@Override
	public boolean equals(Object other)
	{
		return other instanceof TileEntityItemPipe && ((TileEntityItemPipe)other).world.equals(world) && ((TileEntityItemPipe)other).pos.equals(pos);
	}

	@Override
	public boolean canConnectToSide(EnumFacing side)
	{
		TileEntity te = world.getTileEntity(pos.offset(side));
		if (te instanceof TileEntityItemPipe) return canConnectTo((TileEntityItemPipe)te);
		return true;
	}

	@Override
	public void update()
	{
		boolean removedAny = false;
		if (!hasLoaded)
		{
			// While onLoad() exists, it is not safe for tile entities that interact with each other on this scale
			// because chunkloading is unpredictable. update() does not run until all chunks are loaded and therefore is safe.
			load();
			hasLoaded = true;
		}
		
		Set<RoutedItem> toRemove = new HashSet<>();
		Set<RoutedItemClient> toRemoveClient = new HashSet<>();
		for (RoutedItem item : items)
		{
			if (item.tick())
			{
				toRemove.add(item);
				removedAny = true;
			}
		}
		clientSideItems.forEach(item -> {
			if (item.tick()) toRemoveClient.add(item);
		});
		items.removeAll(toRemove); // Can't be done in place because CMEs
		clientSideItems.removeAll(toRemoveClient);
		if (removedAny) onRemoval();
	}
	
	@Override
	public NBTTagCompound writeToNBT(NBTTagCompound nbt)
	{
		super.writeToNBT(nbt);
		NBTTagList list = new NBTTagList();
		items.forEach(item -> list.appendTag(item.writeToNBT(new NBTTagCompound())));
		nbt.setTag("items", list);
		return nbt;
	}
	
	@Override
	public void readFromNBT(NBTTagCompound nbt)
	{
		super.readFromNBT(nbt);
		NBTTagList list = nbt.getTagList("items", 10);
		for (int i=0; i<list.tagCount(); i++)
		{
			RoutedItem ri = new RoutedItem(this, list.getCompoundTagAt(i));
			items.add(ri);
			PacketHandler.sendToAll(new PacketCreateRoutedItem(ri));
		}
	}

//	@Override
//	public NBTTagCompound getUpdateTag()
//	{
//		NBTTagCompound nbt = super.getUpdateTag();
//		nbt.setInteger("colour", colour.id);
//		return nbt;
//	}
//	
//	@Override
//	public SPacketUpdateTileEntity getUpdatePacket()
//	{
//		NBTTagCompound nbt = new NBTTagCompound();
//		nbt.setInteger("colour", colour.id);
//		return new SPacketUpdateTileEntity(pos, 1, nbt);
//	}
//	
//	@Override
//	public void onDataPacket(NetworkManager net, SPacketUpdateTileEntity packet)
//	{
//		colour = EnumColour.getFromID(packet.getNbtCompound().getInteger("colour"));
//	}
	
	public RoutedItem getItem(int id)
	{
		for (RoutedItem item : items)
		{
			if (item.getID() == id) return item;
		}
		return null;
	}
	
	public void removeItem(int id)
	{
		items.removeIf(item -> item.getID() == id);
	}
	
	public int numItems()
	{
		return items.size();
	}
	
	public void onRemoval()
	{
		// noop
	}
	// For TESR
	public Set<RoutedItemClient> getClientItems()
	{
		return clientSideItems;
	}
}