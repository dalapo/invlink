package dalapo.invlink.tileentity;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import dalapo.invlink.auxiliary.BackstuffInventory;
import dalapo.invlink.auxiliary.DummyReceivingInventory;
import dalapo.invlink.auxiliary.IGuiTile;
import dalapo.invlink.auxiliary.IInventoriedTile;
import dalapo.invlink.auxiliary.IUpgradeable;
import dalapo.invlink.auxiliary.Pair;
import dalapo.invlink.client.gui.ContainerBase;
import dalapo.invlink.client.gui.ContainerFilter;
import dalapo.invlink.client.gui.GuiFilter;
import dalapo.invlink.helper.InventoryHelper;
import dalapo.invlink.helper.Logger;
import dalapo.invlink.network.INetworkedObject;
import dalapo.invlink.network.PipeNetwork;
import dalapo.invlink.network.PipeNode;
import dalapo.invlink.network.RoutedItem;
import dalapo.invlink.packet.PacketCreateRoutedItem;
import dalapo.invlink.packet.PacketHandler;
import dalapo.invlink.reference.StateList;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ITickable;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import net.minecraftforge.items.CapabilityItemHandler;
import net.minecraftforge.items.IItemHandler;
import net.minecraftforge.items.ItemStackHandler;

import static net.minecraftforge.items.CapabilityItemHandler.ITEM_HANDLER_CAPABILITY;

// Abstract base class of Transposer, Retriever, etc
public abstract class TileEntityNetworkInserter extends TileEntityBase implements INetworkedObject, ITickable, IInventoriedTile, IGuiTile, IUpgradeable
{
	private static final int TICKS_PER_PUSH = 8;
	
	protected boolean isAutomatic = false;
	protected EnumFacing direction; // Points towards the pipes it pushes into
	protected PipeNetwork network;
	protected BackstuffInventory inventory = new BackstuffInventory(this);
	protected DummyReceivingInventory receiver = new DummyReceivingInventory(inventory);
	protected ItemStackHandler filter = new ItemStackHandler(9) {
		@Override
        protected void onContentsChanged(int slot)
		{
			markDirty();
        }
	};
	private int cooldown = 0;
	
	@Override
	public void onLoad()
	{
		super.onLoad();
		direction = world.getBlockState(pos).getValue(StateList.directions);
	}
	
	@Override
	public void updateState()
	{
		direction = world.getBlockState(pos).getValue(StateList.directions);
	}
	
	@Override
	public boolean hasCapability(Capability<?> c, EnumFacing side)
	{
		EnumFacing dir = world.getBlockState(pos).getValue(StateList.directions);
		return (side != null && side.getAxis().equals(dir.getAxis()) && c == CapabilityItemHandler.ITEM_HANDLER_CAPABILITY);
	}
	
	@Override
	public <T> T getCapability(Capability<T> c, EnumFacing side)
	{
		EnumFacing dir = world.getBlockState(pos).getValue(StateList.directions);
		if (side == dir) return CapabilityItemHandler.ITEM_HANDLER_CAPABILITY.cast(inventory);
		else if (side == dir.getOpposite()) return CapabilityItemHandler.ITEM_HANDLER_CAPABILITY.cast(receiver);
		return null;
	}
	
	public PipeNetwork getNetwork()
	{
		return network;
	}
	
	public void setNetwork(PipeNetwork net)
	{
		this.network = net;
	}

	@Override
	public boolean canConnectToSide(EnumFacing side)
	{
		return direction.getAxis() == side.getAxis();
	}
	
	private void pushNextItem()
	{
		if (world.isAirBlock(pos.offset(direction)))
		{
			world.spawnEntity(new EntityItem(world, pos.offset(direction).getX()+0.5, pos.offset(direction).getY()+0.5, pos.offset(direction).getZ()+0.5, inventory.grabFirstItem(false)));
		}
		else
		{
			TileEntity te = world.getTileEntity(getPos().offset(direction));
			if (te instanceof TileEntityItemPipe)
			{
				TileEntityItemPipe pipe = (TileEntityItemPipe)te;
				ItemStack is = inventory.grabFirstItem(true);
				Pair<PipeNode, EnumFacing> dest = pipe.getNetwork().findSuitableDestination(pipe.getNodes().a, is, false);
				if (dest != null)
				{
					RoutedItem routedItem = new RoutedItem(direction, dest, is);
					LinkedList<PipeNode> path = pipe.getNetwork().pathNodal(pipe.getNodes().a, dest.a);
					routedItem.setPath(path);
					pipe.addItem(routedItem);
					pipe.getNetwork().addItem(routedItem);
					inventory.grabFirstItem(false);
					PacketHandler.sendToAll(new PacketCreateRoutedItem(routedItem));
				}
			}
			else if (te != null && te.hasCapability(ITEM_HANDLER_CAPABILITY, direction))
			{
				ItemStack is = InventoryHelper.tryInsertItem(te.getCapability(ITEM_HANDLER_CAPABILITY, direction), inventory.grabFirstItem(false), false);
				if (!is.isEmpty()) inventory.addItem(is, false);
			}
		}
	}
	
	public void forcePushItem()
	{
		cooldown = 0;
	}
	
	@Override
	public void update()
	{
		if (cooldown-- <= 0)
		{
			if (inventory.getSlots() > 0)
			{
				pushNextItem();
			}
			if (isAutomatic && !world.isBlockPowered(pos))
			{
				onRedstoneEdge();
			}
			cooldown = TICKS_PER_PUSH;
		}
	}
	
	public void makeAutomatic()
	{
		isAutomatic = true;
	}
	
	@Override
	public IItemHandler getInventory()
	{
		return filter;
	}
	
	@Override
	public Container getContainer(EntityPlayer ep)
	{
		return new ContainerFilter(3, 3, ep.inventory, this);
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public GuiScreen getGui(EntityPlayer ep)
	{
		return new GuiFilter((ContainerFilter)getContainer(ep), 176, 166, ep.inventory, this);
	}
	
	@Override
	public NBTTagCompound writeToNBT(NBTTagCompound nbt)
	{
		super.writeToNBT(nbt);
		nbt.setTag("filter", filter.serializeNBT());
		nbt.setBoolean("auto", isAutomatic);
		return nbt;
	}
	
	@Override
	public void readFromNBT(NBTTagCompound nbt)
	{
		super.readFromNBT(nbt);
		if (nbt.hasKey("filter")) filter.deserializeNBT(nbt.getCompoundTag("filter"));
		isAutomatic = nbt.getBoolean("auto");
	}
	
	@Override
	public boolean applyUpgrade(int id)
	{
		if (id == 0 && !isAutomatic)
		{
			isAutomatic = true;
			return true;
		}
		return false;
	}
}