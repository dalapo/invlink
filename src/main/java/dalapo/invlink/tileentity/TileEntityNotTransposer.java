package dalapo.invlink.tileentity;

import static net.minecraftforge.items.CapabilityItemHandler.ITEM_HANDLER_CAPABILITY;

import dalapo.invlink.helper.InventoryHelper;
import dalapo.invlink.helper.Logger;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraftforge.items.IItemHandler;

public class TileEntityNotTransposer extends TileEntityNetworkInserter
{
	@Override
	public void onRedstoneEdge()
	{
		if (!world.isRemote && inventory.getSlots() == 0)
		{
//			Logger.info(String.format("Attempting to get inventory in direction %s at position %s", direction.getOpposite(), pos.offset(direction.getOpposite())));
			TileEntity te = world.getTileEntity(pos.offset(direction.getOpposite()));
			if (te != null && te.hasCapability(ITEM_HANDLER_CAPABILITY, direction))
			{
				IItemHandler inv = te.getCapability(ITEM_HANDLER_CAPABILITY, direction);
				ItemStack is = InventoryHelper.extractFirstItem(inv, filter);
				if (!is.isEmpty()) inventory.addItem(is, true);
				else Logger.info("Empty inventory.");
			}
		}
	}
}