package dalapo.invlink.tileentity;

import net.minecraft.block.state.IBlockState;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.play.server.SPacketUpdateTileEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public abstract class TileEntityBase extends TileEntity
{
	public boolean isPowered = false;
	
	public void onLoad()
	{
		isPowered = world.isBlockPowered(pos);
	}
	
	/**
	 * Called when the Tile Entity receives a Redstone signal and isn't already powered.
	 */
	public void onRedstoneEdge()
	{
		// no-op
	}
	
	public void onNeighbourUpdate(EnumFacing f)
	{
		// no-op
	}
	
	public void updateState()
	{
		// no-op
	}
	
	public void remove()
	{
		// no-op
	}
	
	@Override
	public boolean shouldRefresh(World world, BlockPos pos, IBlockState oldState, IBlockState newState)
	{
		return oldState.getBlock() != newState.getBlock();
	}
	
	public int getField(int id)
	{
		return 0; // default
	}
	
	public void setField(int id, int val)
	{
		// noop
	}
	
	public void toggleField(int id)
	{
		// noop
	}

	public int getFieldCount()
	{
		return 0;
	}
	
	@Override
	public NBTTagCompound getUpdateTag()
	{
		NBTTagCompound nbt = super.getUpdateTag();
		return writeToNBT(nbt);
	}
	
	@Override
	public SPacketUpdateTileEntity getUpdatePacket()
	{
		NBTTagCompound nbt = new NBTTagCompound();
		this.writeToNBT(nbt);
		return new SPacketUpdateTileEntity(getPos(), 1, nbt);
	}
	
	@Override
	public void onDataPacket(NetworkManager net, SPacketUpdateTileEntity packet)
	{
//		Logger.info(String.format("Entered onDataPacket; thread = %s", Thread.currentThread()));
		this.readFromNBT(packet.getNbtCompound());
	}
}