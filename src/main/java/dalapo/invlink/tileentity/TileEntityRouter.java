package dalapo.invlink.tileentity;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import javax.annotation.Nullable;

import dalapo.invlink.InventoryLink;
import dalapo.invlink.auxiliary.IGuiTile;
import dalapo.invlink.auxiliary.IInventoriedTile;
import dalapo.invlink.auxiliary.Pair;
import dalapo.invlink.auxiliary.RouterTileRegistry;
import dalapo.invlink.client.gui.ContainerInventory;
import dalapo.invlink.client.gui.GuiBasicContainer;
import dalapo.invlink.client.gui.widget.WidgetToggleSwitch;
import dalapo.invlink.reference.StateList;
import dalapo.invlink.helper.BlockHelper;
import dalapo.invlink.helper.InventoryHelper;
import dalapo.invlink.helper.Logger;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.nbt.NBTTagLong;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.tileentity.TileEntityChest;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ITickable;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockPos.MutableBlockPos;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.event.world.BlockEvent.BreakEvent;
import net.minecraftforge.event.world.BlockEvent.EntityPlaceEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.items.CapabilityItemHandler;
import net.minecraftforge.items.IItemHandler;
import net.minecraftforge.items.ItemStackHandler;

import static dalapo.invlink.helper.InvLinkMathHelper.isInRangeIncl;
import static dalapo.invlink.helper.BlockHelper.getDirection;

// Works like FZ Router for the most part, but has a built-in, forced machine filter.
/**
 * @see dalapo.invlink.auxiliary.RouterTileRegistry
 */
public class TileEntityRouter extends TileEntityBase implements ITickable, IGuiTile, IInventoriedTile
{
	private Class<? extends TileEntity> tileType;
	private List<BlockPos> connectedTiles = new ArrayList<>();
	private boolean extract = true; // true: insert into inventories / false: extract from inventories
	private ItemStackHandler inventory = new ItemStackHandler(1) {
		@Override
		protected void onContentsChanged(int slot)
		{
			markDirty();
		}
	};
	private boolean needsRecalc = false;
	
	public TileEntityRouter()
	{
		MinecraftForge.EVENT_BUS.register(this);
	}
	
	private void recalcHelper(BlockPos cur)
	{
		TileEntity te = world.getTileEntity(cur);
		connectedTiles.add(cur);
		for (EnumFacing f : EnumFacing.values())
		{
			if (!connectedTiles.contains(cur.offset(f)))
			{
				TileEntity next = world.getTileEntity(cur.offset(f));
				if (next != null && next.getClass() == tileType)
				{
					recalcHelper(cur.offset(f));
				}
			}
		}
	}
	
	private void clearTiles()
	{
		connectedTiles.clear();
	}
	
	public void recalc()
	{
		Logger.info("Running recalc");
		clearTiles();
		TileEntity te = world.getTileEntity(pos.offset(getDirection(world, pos)));
		if (te != null && te.hasCapability(CapabilityItemHandler.ITEM_HANDLER_CAPABILITY, getDirection(world, pos).getOpposite()))
		{
			tileType = te.getClass();
			recalcHelper(pos.offset(getDirection(world, pos)));
		}
		Logger.info(String.format("Currently connected to %s tiles", connectedTiles.size()));
	}
	
	@SubscribeEvent
	public void handleBreak(BreakEvent evt)
	{
		if (connectedTiles.contains(evt.getPos())) needsRecalc = true;
	}
	
	@SubscribeEvent
	public void handlePlace(EntityPlaceEvent evt)
	{
		for (EnumFacing f : EnumFacing.values())
		{
			BlockPos bp = evt.getPos().offset(f);
			if (connectedTiles.contains(bp) || bp.equals(pos)) needsRecalc = true;
		}
	}
	
	@Override
	public void update()
	{
		if (!world.isRemote)
		{
			if (needsRecalc)
			{
				recalc();
				needsRecalc = false;
			}
			
			if (!connectedTiles.isEmpty() && !needsRecalc)
			{
				TileEntity te = world.getTileEntity(connectedTiles.get(InventoryLink.random.nextInt(connectedTiles.size())));
				if (te != null)
				{
					IItemHandler inv = te.getCapability(CapabilityItemHandler.ITEM_HANDLER_CAPABILITY, getDirection(world, pos).getOpposite());
	//			IItemHandler inv = world.getTileEntity(connectedTiles.get(InventoryLink.random.nextInt(connectedTiles.size()))).getCapability(CapabilityItemHandler.ITEM_HANDLER_CAPABILITY, getDirection(world, pos).getOpposite());
					if (extract)
					{
						ItemStack toInsert = inventory.extractItem(0, 1, true);
						if (InventoryHelper.tryInsertItem(inv, toInsert, false).isEmpty()) inventory.extractItem(0, 1, false);
					}
					else
					{
						for (int i=0; i<inv.getSlots(); i++)
						{
							ItemStack toExtract = inv.extractItem(i, 1, true);
							if (!toExtract.isEmpty() && InventoryHelper.tryInsertItem(inventory, toExtract, false).isEmpty())
							{
								inv.extractItem(i, 1, false);
								break;
							}
						}
					}
				}
			}
		}
	}
	
	@Override
	public Container getContainer(EntityPlayer ep)
	{
		return new ContainerInventory(1, 1, ep.inventory, this);
	}
	
	@Override
	public GuiScreen getGui(EntityPlayer ep)
	{
		GuiBasicContainer gui = new GuiBasicContainer("router", (ContainerInventory)getContainer(ep), 176, 166, ep.inventory, this);
		gui.addWidget(new WidgetToggleSwitch(gui, 0, 140, 20, "Extract", "Insert"));
		return gui;
	}

	@Override
	public IItemHandler getInventory()
	{
		return inventory;
	}
	
	@Override
	public void updateState()
	{
		recalc();
	}
	
	@Override
	public boolean hasCapability(Capability<?> cap, EnumFacing side)
	{
		return (cap == CapabilityItemHandler.ITEM_HANDLER_CAPABILITY && side != getDirection(world, pos));
	}
	
	@Override
	@Nullable
	public <T> T getCapability(Capability<T> cap, EnumFacing side)
	{
		if (hasCapability(cap, side)) return CapabilityItemHandler.ITEM_HANDLER_CAPABILITY.cast(inventory);
		return null;
	}
	
	@Override
	public NBTTagCompound writeToNBT(NBTTagCompound nbt)
	{
		super.writeToNBT(nbt);
		nbt.setBoolean("extracting", extract);
		NBTTagList positions = new NBTTagList();
		for (BlockPos bp : connectedTiles)
		{
			positions.appendTag(new NBTTagLong(bp.toLong()));
		}
		nbt.setTag("positions", positions);
		nbt.setTag("inventory", inventory.serializeNBT());
		return nbt;
	}
	
	@Override
	public void readFromNBT(NBTTagCompound nbt)
	{
		super.readFromNBT(nbt);
		extract = nbt.getBoolean("extracting");
		NBTTagList positionsList = nbt.getTagList("positions", 4);
		positionsList.forEach(nbtTag -> connectedTiles.add(BlockPos.fromLong(((NBTTagLong)nbtTag).getLong())));
		inventory.deserializeNBT(nbt.getCompoundTag("inventory"));
	}
	
	@Override
	public void invalidate()
	{
		super.invalidate();
		MinecraftForge.EVENT_BUS.unregister(this);
	}

	@Override
	public int getFieldCount()
	{
		return 1;
	}
	
	@Override
	public int getField(int id)
	{
		if (id == 0) return extract ? 1 : 0;
		return 0;
	}
	
	@Override
	public void setField(int id, int val)
	{
		switch (id)
		{
		case 0:
			extract = (val != 0);
		}
	}
	
	@Override
	public void toggleField(int id)
	{
		switch (id)
		{
		case 0:
			extract = !extract;
		}
	}
}