package dalapo.invlink.network;

import java.util.Set;

import net.minecraft.util.EnumFacing;

public interface INetworkedObject
{
	public PipeNetwork getNetwork();
	public void setNetwork(PipeNetwork net);
	public boolean canConnectToSide(EnumFacing side);
}