package dalapo.invlink.network;

import net.minecraft.util.math.BlockPos;

public interface INetworkInserter
{
	public BlockPos getPos();
}