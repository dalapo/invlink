package dalapo.invlink.network;

import java.util.ArrayList;
import java.util.List;

import dalapo.invlink.tileentity.TileEntityItemPipe;

public class PipeEdge
{
	private PipeNode origin;
	private PipeNode dest;
	private TileEntityItemPipe immediateConnection;
	private List<TileEntityItemPipe> pipes = new ArrayList<>(); // Includes dest node, but not origin
}