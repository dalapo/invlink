package dalapo.invlink.network;

import java.util.LinkedList;
import java.util.List;

import dalapo.invlink.InventoryLink;
import dalapo.invlink.auxiliary.BackstuffInventory;
import dalapo.invlink.auxiliary.Pair;
import dalapo.invlink.helper.InvLinkMathHelper;
import dalapo.invlink.helper.InventoryHelper;
import dalapo.invlink.helper.Logger;
import dalapo.invlink.helper.NetworkHelper;
import dalapo.invlink.packet.PacketCreateRoutedItem;
import dalapo.invlink.packet.PacketHandler;
import dalapo.invlink.packet.PacketUpdateRoutedItem;
import dalapo.invlink.tileentity.TileEntityItemPipe;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagInt;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.items.IItemHandler;

public class RoutedItem
{
	public static final int TICKS = 8; // Time taken for item to go from edge to centre or centre to edge

	private int age;
	private int id;
	private boolean hasID = false;
	private EnumFacing currentDir;
	private EnumFacing nextDir;
	private TileEntityItemPipe currentPipe;
	private ItemStack item;
	private boolean isDirty = false;
	private boolean isBackstuffing = false;
	private LinkedList<PipeNode> nodeSteps;
	private LinkedList<EnumFacing> steps;
	private Pair<PipeNode, EnumFacing> destination = new Pair<>();
	// Have to implement backstuffing somehow
	
	public RoutedItem(EnumFacing initialDir, Pair<PipeNode, EnumFacing> dest, ItemStack is)
	{
		this.item = is;
		this.currentDir = initialDir;
		this.destination = dest;
		age = -TICKS;
	}
	
	public RoutedItem(TileEntityItemPipe p, NBTTagCompound nbt)
	{
		setPipe(p);
		readFromNBT(nbt);
	}
	
	public void setID(int id)
	{
		if (!hasID) this.id = id; // Make ID pseudo-final without enforcing setting a value on the constructor
		hasID = true;
	}
	
	public int getID()
	{
		return id;
	}
	
	public boolean hasID()
	{
		return hasID;
	}
	
	public void setPath(LinkedList<PipeNode> path)
	{
		nodeSteps = path;
		setPathDirections(NetworkHelper.convertNodeList(path));
	}

	public void setPathDirections(LinkedList<EnumFacing> path)
	{
		this.steps = path;
//		Logger.info(String.format("%s, %s", nodeSteps.size(), steps.size()));
	}
	
	public void markDirty()
	{
		isDirty = true;
	}
	
	public void markDirtyIfContains(PipeNode p)
	{
		if (nodeSteps.contains(p)) markDirty();
	}
	
	private void recalcSteps()
	{
		Pair<PipeNode, EnumFacing> dest = currentPipe.getNetwork().findSuitableDestination(currentPipe.getNodes().a, item, true);
		if (dest != null)
		{
			this.destination = dest;
			setPath(currentPipe.getNetwork().pathNodal(currentPipe.getNodes().a, dest.a));
		}
		isDirty = false;
//		updateDirections();
	}
	
	/**
	 * something something do not modify the returned itemstack
	 */
	public ItemStack getItem()
	{
		return item;
	}
	
	public LinkedList<EnumFacing> getSteps()
	{
		return steps;
	}
	
	public BlockPos getPos()
	{
		return currentPipe.getPos();
	}
	
	public void setPipe(TileEntityItemPipe pipe)
	{
		currentPipe = pipe;
	}
	
	public EnumFacing getDir()
	{
		return currentDir;
	}
	
	public EnumFacing getNext()
	{
		return nextDir;
	}
	
	public void updateDirections()
	{
		if (currentPipe.shouldBeNode())
		{
			if (isDirty)
			{
				recalcSteps();
			}
			
			if (destination == null)
			{
				for (EnumFacing f : EnumFacing.VALUES)
				{
					if (currentPipe.getConnectionDirections().contains(f))
					{
						nextDir = f;
						break;
					}
				}
			}
			else
			{
				if (currentPipe.getNodes().a != destination.a && !nodeSteps.isEmpty())
				{
	//				Logger.info(nodeSteps);
	//				nextDir = null;
					nextDir = currentPipe.getNodes().a.getImmediateDirection(nodeSteps.pop());
				}
				else nextDir = destination.b;
			}
		}
		else
		{
			nextDir = currentPipe.getOtherConnection(currentDir.getOpposite());
		}
	}
	
	// Returns true if the item should be removed from the pipe.
	// Only runs server-side.
	public boolean tick()
	{
		if (++age >= TICKS)
		{
			age = -TICKS;
			if (currentPipe.isNode() && currentPipe.getNodes().a == destination.a)
			{
				IItemHandler inv = currentPipe.getInventories().get(destination.b);
				if (inv instanceof BackstuffInventory)
				{
					((BackstuffInventory)inv).addItem(item, false);
					return true;
				}
				else
				{
					item = InventoryHelper.tryInsertItem(inv, item, false);
					if (!item.isEmpty())
					{
						currentDir = destination.b.getOpposite();
						isDirty = true;
						currentPipe.addItem(this);
						return false;
					}
					else return true;
				}
			}
			
			if (nextDir != null)
			{
				currentDir = nextDir;
				TileEntityItemPipe nextPipe = currentPipe.getConnectedPipe(nextDir);
				if (nextPipe == null)
				{
					if (!currentPipe.getWorld().isRemote)
					{
						EntityItem ei = new EntityItem(currentPipe.getWorld(), currentPipe.getPos().getX()+0.5, currentPipe.getPos().getY()+0.5, currentPipe.getPos().getZ()+0.5, item);
						currentPipe.getWorld().spawnEntity(ei);
					}
				}
//				else if (currentPipe.hasTeleport(nextDir) && InventoryLink.random.nextInt(10) < 2) return true; // have a chance to destroy an item instead of teleporting it
				else currentPipe.getConnectedPipe(nextDir).addItem(this);
			}
			else
			{
				currentPipe.addItem(this);
			}
			return true;
		}
		return false;
	}
	
	public int getAge()
	{
		return age;
	}
	
	public NBTTagCompound writeToNBT(NBTTagCompound nbt)
	{
		nbt.setInteger("id", id);
		nbt.setInteger("age", age);
		nbt.setInteger("dir", currentDir.getIndex());
		nbt.setInteger("next", nextDir.getIndex());
		nbt.setInteger("destdir", destination.b.getIndex());
		nbt.setLong("destnode", destination.a.getTile().getPos().toLong());
//		NBTTagList list = new NBTTagList();
//		for (EnumFacing f : steps)
//		{
//			list.appendTag(new NBTTagInt(f.getIndex()));
//		}
//		nbt.setTag("path", list);
		NBTTagCompound compound = item.serializeNBT();
		nbt.setTag("item", compound);
		return nbt;
	}
	
	public void readFromNBT(NBTTagCompound nbt)
	{
		setID(nbt.getInteger("id"));
		age = nbt.getInteger("age");
		currentDir = EnumFacing.getFront(nbt.getInteger("dir"));
		nextDir = EnumFacing.getFront(nbt.getInteger("next"));
		steps = new LinkedList<>();
		nodeSteps = new LinkedList<>();
		destination = new Pair<>(null, EnumFacing.getFront(nbt.getInteger("destdir"))); // look away, look away
		isDirty = true;
//		NBTTagList list = nbt.getTagList("path", 3);
//		steps = new LinkedList<>();
//		for (int i=0; i<list.tagCount(); i++)
//		{
//			steps.add(EnumFacing.getFront(list.getIntAt(i)));
//		}
		item = new ItemStack(nbt.getCompoundTag("item"));
	}

}