package dalapo.invlink.network;

import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumFacing;

public class RoutedItemClient
{
	public final ItemStack is;
	public final EnumFacing entryDir;
	public final EnumFacing exitDir;
	public int age;
	
	public RoutedItemClient(ItemStack is, EnumFacing entry, EnumFacing exit)
	{
		this.is = is;
		entryDir = entry;
		exitDir = exit;
		age = -RoutedItem.TICKS;
	}
	
	public boolean tick()
	{
		return ++age > RoutedItem.TICKS;
	}
}