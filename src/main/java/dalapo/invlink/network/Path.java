package dalapo.invlink.network;

import java.util.LinkedList;

public class Path
{
	private PipeNode origin;
	private PipeNode dest;
	private LinkedList<PipeNode> steps;
	private int length;
}