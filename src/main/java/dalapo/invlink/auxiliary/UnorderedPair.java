package dalapo.invlink.auxiliary;

public class UnorderedPair<T> extends Pair<T, T>
{
	public UnorderedPair(T a, T b)
	{
		super(a, b);
	}
	
	@Override
	public boolean equals(Object o)
	{
		return (this == o || o instanceof Pair && ((Pair)o).isEquivalent(this));
	}
}