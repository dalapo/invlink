package dalapo.invlink.auxiliary;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

public class SortedLinkedList<T extends Comparable<T>>
{
	private SortedLinkedListNode head;
	private SortedLinkedListNode tail;
	private class SortedLinkedListNode
	{
		private SortedLinkedListNode prev;
		private SortedLinkedListNode next;
		private T object;
		
		SortedLinkedListNode(T obj)
		{
			object = obj;
		}
		
		void addBefore(T other)
		{
			SortedLinkedListNode node = new SortedLinkedListNode(other);
			node.prev = this.prev;
			node.next = this;
			this.prev = node;
		}
		
		void addAfter(T other)
		{
			SortedLinkedListNode node = new SortedLinkedListNode(other);
			node.prev = this;
			node.next = this.next;
			this.next = node;
		}
		
		void remove()
		{
			prev.next = this.next;
			next.prev = this.prev;
		}
	}
}