package dalapo.invlink.auxiliary;

import net.minecraft.item.ItemStack;

public interface ISpecialItem
{
	public String getName(ItemStack is);
}