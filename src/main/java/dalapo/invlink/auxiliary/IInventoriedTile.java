package dalapo.invlink.auxiliary;

import net.minecraftforge.items.IItemHandler;

public interface IInventoriedTile
{
	public IItemHandler getInventory();
}