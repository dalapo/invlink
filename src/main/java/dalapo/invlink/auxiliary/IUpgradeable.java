package dalapo.invlink.auxiliary;

public interface IUpgradeable
{
	/**
	 * Returns true if the upgrade was successfully applied.
	 * @param id
	 * @return
	 */
	public boolean applyUpgrade(int id);
}