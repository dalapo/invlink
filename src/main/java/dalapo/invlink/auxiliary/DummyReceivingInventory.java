package dalapo.invlink.auxiliary;

import dalapo.invlink.helper.Logger;
import net.minecraft.item.ItemStack;
import net.minecraftforge.items.IItemHandler;

/**
 * Dummy inventory for the front of network inserters. Accepts items and sends them directly to the backstuff,
 * but only if the backstuff is empty.
 * @author davidpowell
 *
 */
public class DummyReceivingInventory implements IItemHandler
{
	private BackstuffInventory backstuff;

	public DummyReceivingInventory(BackstuffInventory back)
	{
		backstuff = back;
	}
	
	@Override
	public int getSlots()
	{
		return 1;
	}

	@Override
	public ItemStack getStackInSlot(int slot)
	{
		return ItemStack.EMPTY;
	}

	@Override
	public ItemStack insertItem(int slot, ItemStack stack, boolean simulate)
	{
		if (backstuff.getSlots() == 0)
		{
			if (!simulate) backstuff.addItem(stack, true);
			return ItemStack.EMPTY;
		}
		return stack;
	}

	@Override
	public ItemStack extractItem(int slot, int amount, boolean simulate)
	{
		return ItemStack.EMPTY;
	}

	@Override
	public int getSlotLimit(int slot)
	{
		return 64;
	}
}