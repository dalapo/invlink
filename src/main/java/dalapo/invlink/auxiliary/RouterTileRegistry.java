package dalapo.invlink.auxiliary;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import dalapo.invlink.tileentity.TileEntityRouter;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraftforge.common.util.BlockSnapshot;
import net.minecraftforge.items.IItemHandler;

public class RouterTileRegistry
{
	private RouterTileRegistry() {}
	
	public static final RouterTileRegistry instance = new RouterTileRegistry();
	
	private Map<BlockPos, Set<TileEntityRouter>> routerMap = new HashMap<>();
	
	public void addRouter(TileEntityRouter router, Set<BlockPos> connectedTiles)
	{
		
	}
	
	public void removeRouter(TileEntityRouter router)
	{
		routerMap.entrySet().forEach(entry -> {
			entry.getValue().remove(router);
		});
		routerMap.entrySet().removeIf(entry -> entry.getValue().isEmpty());
	}
	
	public void handleBreak(BlockPos pos)
	{
		if (routerMap.containsKey(pos))
		{
			routerMap.get(pos).forEach(router -> router.recalc());
		}
	}
	
	public void handlePlace(BlockPos pos)
	{
		if (routerMap.containsKey(pos))
		{
			routerMap.get(pos).forEach(router -> router.recalc()); // fine
		}
	}
}