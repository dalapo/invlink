package dalapo.invlink.auxiliary;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import dalapo.invlink.tileentity.TileEntityNetworkInserter;
import net.minecraft.item.ItemStack;
import net.minecraft.util.NonNullList;
import net.minecraftforge.items.IItemHandler;

public class BackstuffInventory implements IItemHandler
{
	LinkedList<ItemStack> items = new LinkedList<>();
	TileEntityNetworkInserter parent;
	
	public BackstuffInventory(TileEntityNetworkInserter parent)
	{
		this.parent = parent;
	}
	
	@Override
	public int getSlots()
	{
		return items.size();
	}

	@Override
	public ItemStack getStackInSlot(int slot)
	{
		return items.get(slot);
	}

	// If signal is true, immediately alert the
	public void addItem(ItemStack stack, boolean signal)
	{
		items.add(stack);
		if (signal) parent.forcePushItem();
	}
	
	@Override
	public ItemStack insertItem(int slot, ItemStack stack, boolean simulate)
	{
		if (!simulate) items.add(slot, stack.copy());
		return ItemStack.EMPTY;
	}

	@Override
	public ItemStack extractItem(int slot, int amount, boolean simulate)
	{
		ItemStack is = items.get(slot).copy();
		is.setCount(Math.min(amount, is.getCount()));
		if (!simulate) items.get(slot).shrink(amount);
		if (items.get(slot).isEmpty()) items.remove(slot);
		return is;
	}
	
	public ItemStack grabFirstItem(boolean simulate)
	{
		if (simulate) return items.peek();
		else return items.pop();
	}

	@Override
	public int getSlotLimit(int slot)
	{
		return 64;
	}
}