package dalapo.invlink.auxiliary;

import net.minecraft.client.gui.GuiScreen;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.Container;

public interface IGuiTile
{
	public Container getContainer(EntityPlayer ep);
	public GuiScreen getGui(EntityPlayer ep);
}