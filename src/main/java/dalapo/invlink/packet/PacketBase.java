package dalapo.invlink.packet;

import io.netty.buffer.ByteBuf;
import net.minecraft.client.Minecraft;
import net.minecraft.client.network.NetHandlerPlayClient;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.network.NetHandlerPlayServer;
import net.minecraft.util.IThreadListener;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;
import net.minecraftforge.fml.relauncher.Side;

public abstract class PacketBase implements IMessage
{
	public PacketBase() {}
	
	protected PacketBase getReply()
	{
		return null;
	}
	
	protected abstract void handleClient(PacketBase msg, World world, EntityPlayer ep);
	protected abstract void handleServer(PacketBase msg, World world, EntityPlayer ep);
	
	private final void doHandle(NetHandlerPlayClient nhpc)
	{
		handleClient(this, Minecraft.getMinecraft().world, Minecraft.getMinecraft().player);
	}
	
	private final void doHandle(NetHandlerPlayServer nhps)
	{
		handleServer(this, nhps.player.getEntityWorld(), nhps.player);
	}
	
	public static class Handler implements IMessageHandler<PacketBase, PacketBase>
	{
		@Override
		public PacketBase onMessage(final PacketBase message, MessageContext ctx)
		{
			IThreadListener worldThread = FMLCommonHandler.instance().getWorldThread(ctx.netHandler);
			if (ctx.side.equals(Side.CLIENT))
			{
				worldThread.addScheduledTask(() -> message.doHandle(ctx.getClientHandler()));
			}
			else
			{
				worldThread.addScheduledTask(() -> message.doHandle(ctx.getServerHandler()));
			}
			return message.getReply();
		}
	}
}