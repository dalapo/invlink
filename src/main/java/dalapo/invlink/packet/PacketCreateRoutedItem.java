package dalapo.invlink.packet;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import dalapo.invlink.helper.Logger;
import dalapo.invlink.network.RoutedItem;
import dalapo.invlink.network.RoutedItemClient;
import dalapo.invlink.tileentity.TileEntityItemPipe;
import io.netty.buffer.ByteBuf;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.network.PacketBuffer;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class PacketCreateRoutedItem extends PacketBase
{
	private ItemStack is;
	private BlockPos pos;
	private EnumFacing inDir;
	private EnumFacing outDir;
	
	public PacketCreateRoutedItem(RoutedItem item)
	{
		is = item.getItem();
		pos = item.getPos();
		inDir = item.getDir();
		outDir = item.getNext();
	}
	
	public PacketCreateRoutedItem() {}
	
	@Override
	public void fromBytes(ByteBuf buf)
	{
		PacketBuffer buffer = new PacketBuffer(buf);
		try {
			is = buffer.readItemStack();
			pos = buffer.readBlockPos();
			inDir = EnumFacing.getFront(buffer.readByte());
			byte b = buffer.readByte();
			outDir = b == 99 ? null : EnumFacing.getFront(b);
		}
		catch (IOException e)
		{
			throw new RuntimeException(e);
		}
	}

	@Override
	public void toBytes(ByteBuf buf)
	{
		PacketBuffer buffer = new PacketBuffer(buf);
		buffer.writeItemStack(is);
		buffer.writeBlockPos(pos);
		buffer.writeByte(inDir.getIndex());
		buffer.writeByte(outDir == null ? 99 : outDir.getIndex());
	}

	@Override
	protected void handleClient(PacketBase msg, World world, EntityPlayer ep)
	{
		PacketCreateRoutedItem packet = (PacketCreateRoutedItem)msg;
		RoutedItemClient item = new RoutedItemClient(packet.is, packet.inDir, packet.outDir);
		TileEntityItemPipe te = (TileEntityItemPipe)world.getTileEntity(packet.pos);
		te.getClientItems().add(item);
	}

	@Override
	protected void handleServer(PacketBase msg, World world, EntityPlayer ep)
	{
		// no-op
	}
}