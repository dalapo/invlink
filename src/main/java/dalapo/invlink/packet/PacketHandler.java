package dalapo.invlink.packet;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraftforge.fml.common.network.NetworkRegistry;
import net.minecraftforge.fml.common.network.simpleimpl.SimpleNetworkWrapper;
import net.minecraftforge.fml.relauncher.Side;

public class PacketHandler
{
	private static int packetID = 0;
	
	private static SimpleNetworkWrapper INSTANCE = null;
	
	public static int nextID()
	{
		return packetID++;
	}
	
	private static void registerMessage(Class handler, Class packet, Side side)
	{
		INSTANCE.registerMessage(handler, packet, nextID(), side);
	}
	
	public static void registerMessages(String channelName)
	{
		INSTANCE = NetworkRegistry.INSTANCE.newSimpleChannel(channelName);
		registerMessages();
	}
	
	private static void registerMessages()
	{
		registerMessage(PacketCreateRoutedItem.Handler.class, PacketCreateRoutedItem.class, Side.CLIENT);
		registerMessage(PacketUpdateRoutedItem.Handler.class, PacketUpdateRoutedItem.class, Side.CLIENT);
		registerMessage(PacketSendChatInfo.Handler.class, PacketSendChatInfo.class, Side.CLIENT);
		registerMessage(PacketUpgradeTile.Handler.class, PacketUpgradeTile.class, Side.CLIENT);
		registerMessage(PacketToggleField.Handler.class, PacketToggleField.class, Side.SERVER);
	}
	
	public static void sendToPlayer(PacketBase packet, EntityPlayer player)
	{
		if (player instanceof EntityPlayerMP) INSTANCE.sendTo(packet, (EntityPlayerMP)player);
	}
	
	public static void sendToAll(PacketBase packet)
	{
		INSTANCE.sendToAll(packet);
	}
	
	public static void sendToServer(PacketBase packet)
	{
		INSTANCE.sendToServer(packet);
	}
}