package dalapo.invlink.packet;

import dalapo.invlink.auxiliary.IUpgradeable;
import io.netty.buffer.ByteBuf;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class PacketUpgradeTile extends PacketBase
{
	BlockPos tilePos;
	int upgradeID;
	
	public PacketUpgradeTile(BlockPos pos, int id)
	{
		tilePos = pos;
		upgradeID = id;
	}
	
	public PacketUpgradeTile() {}
	
	@Override
	public void fromBytes(ByteBuf buf)
	{
		tilePos = BlockPos.fromLong(buf.readLong());
		upgradeID = buf.readInt();
	}

	@Override
	public void toBytes(ByteBuf buf)
	{
		buf.writeLong(tilePos.toLong());
		buf.writeInt(upgradeID);
	}

	@Override
	protected void handleClient(PacketBase msg, World world, EntityPlayer ep)
	{
		PacketUpgradeTile packet = (PacketUpgradeTile)msg;
		IUpgradeable te = (IUpgradeable)world.getTileEntity(packet.tilePos);
		te.applyUpgrade(packet.upgradeID);
	}

	@Override
	protected void handleServer(PacketBase msg, World world, EntityPlayer ep)
	{
		// TODO Auto-generated method stub
		
	}

}
