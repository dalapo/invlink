package dalapo.invlink.packet;

import dalapo.invlink.client.gui.ContainerBase;
import dalapo.invlink.tileentity.TileEntityBase;
import io.netty.buffer.ByteBuf;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class PacketToggleField extends PacketBase
{
	BlockPos bp;
	int id;
	
	public PacketToggleField(BlockPos bp, int id)
	{
		this.bp = bp;
		this.id = id;
	}
	
	public PacketToggleField() {}
	
	@Override
	public void fromBytes(ByteBuf buf)
	{
		bp = BlockPos.fromLong(buf.readLong());
		id = buf.readInt();
	}

	@Override
	public void toBytes(ByteBuf buf)
	{
		buf.writeLong(bp.toLong());
		buf.writeInt(id);
	}

	@Override
	protected void handleClient(PacketBase msg, World world, EntityPlayer ep)
	{
		// no-op
	}

	@Override
	protected void handleServer(PacketBase msg, World world, EntityPlayer ep)
	{
		PacketToggleField packet = (PacketToggleField)msg;
		TileEntityBase te = ((ContainerBase)ep.openContainer).getTile();
		te.toggleField(packet.id);
		ep.openContainer.detectAndSendChanges();
	}
	
}