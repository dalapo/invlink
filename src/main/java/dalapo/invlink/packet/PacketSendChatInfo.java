package dalapo.invlink.packet;

import dalapo.invlink.helper.ChatHelper;
import io.netty.buffer.ByteBuf;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.network.PacketBuffer;
import net.minecraft.world.World;

public class PacketSendChatInfo extends PacketBase
{
	String message;
	
	public PacketSendChatInfo(String msg)
	{
		this.message = msg;
	}
	
	public PacketSendChatInfo() {}
	
	@Override
	public void fromBytes(ByteBuf buf)
	{
		PacketBuffer buffer = new PacketBuffer(buf);
		int length = buffer.readInt();
		message = buffer.readString(length);
	}

	@Override
	public void toBytes(ByteBuf buf)
	{
		PacketBuffer buffer = new PacketBuffer(buf);
		buffer.writeInt(message.length());
		buffer.writeString(message);
	}

	@Override
	protected void handleClient(PacketBase msg, World world, EntityPlayer ep)
	{
		ChatHelper.sendChatToPlayer(ep, ((PacketSendChatInfo)msg).message);
	}

	@Override
	protected void handleServer(PacketBase msg, World world, EntityPlayer ep)
	{
		// no-op
	}
}