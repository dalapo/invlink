package dalapo.invlink.packet;

import java.util.LinkedList;
import java.util.List;

import dalapo.invlink.helper.Logger;
import dalapo.invlink.network.RoutedItem;
import dalapo.invlink.tileentity.TileEntityItemPipe;
import io.netty.buffer.ByteBuf;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

/**
 * Packet to update a routed item that <i>already exists.</i>
 * @author davidpowell
 *
 */
public class PacketUpdateRoutedItem extends PacketBase
{
	BlockPos pipe;
	int itemID;
	LinkedList<EnumFacing> newSteps = new LinkedList<>();
	
	public PacketUpdateRoutedItem(RoutedItem item)
	{
		pipe = item.getPos();
		itemID = item.getID();
		newSteps = item.getSteps();
	}
	
	public PacketUpdateRoutedItem() {}
	
	@Override
	public void fromBytes(ByteBuf buf)
	{
		pipe = BlockPos.fromLong(buf.readLong());
		itemID = buf.readInt();
		int size = buf.readInt();
		for (int i=0; i<size; i++)
		{
			newSteps.add(EnumFacing.getFront(buf.readByte()));
		}
	}

	@Override
	public void toBytes(ByteBuf buf)
	{
		buf.writeLong(pipe.toLong());
		buf.writeInt(itemID);
		buf.writeInt(newSteps.size());
		for (EnumFacing f : newSteps)
		{
			buf.writeByte(f.getIndex());
		}
	}

	@Override
	protected void handleClient(PacketBase msg, World world, EntityPlayer ep)
	{
		PacketUpdateRoutedItem packet = (PacketUpdateRoutedItem)msg;
		TileEntityItemPipe te = (TileEntityItemPipe)world.getTileEntity(packet.pipe);
		RoutedItem item = te.getItem(packet.itemID);
		item.setPathDirections(packet.newSteps);
	}

	@Override
	protected void handleServer(PacketBase msg, World world, EntityPlayer ep)
	{
		
	}
}