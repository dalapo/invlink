package dalapo.invlink;

import java.awt.Toolkit;

import dalapo.invlink.auxiliary.BlockUpdateListener;
import dalapo.invlink.client.render.PipeModelLoader;
import dalapo.invlink.helper.Logger;
import dalapo.invlink.init.BlockRegistry;
import dalapo.invlink.init.ItemRegistry;
import dalapo.invlink.init.TileRegistry;
import net.minecraftforge.client.event.ModelRegistryEvent;
import net.minecraftforge.client.model.ModelLoaderRegistry;
import net.minecraftforge.event.world.WorldEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.relauncher.SideOnly;
import net.minecraftforge.fml.relauncher.Side;

@SideOnly(Side.CLIENT)
@Mod.EventBusSubscriber(Side.CLIENT)
public class ClientProxy extends CommonProxy
{
	public void preInit(FMLPreInitializationEvent evt)
	{
		super.preInit(evt);
		ModelLoaderRegistry.registerLoader(new PipeModelLoader());
	}
	
	public void init(FMLInitializationEvent evt)
	{
		super.init(evt);
		BlockRegistry.pipe.initModel();
		TileRegistry.registerTESRs();
	}
	
	public void postInit(FMLPostInitializationEvent evt)
	{
		super.postInit(evt);
		BlockRegistry.registerItemModels();
	}
	
	@SubscribeEvent
	public void registerModels(ModelRegistryEvent evt)
	{
		BlockRegistry.initModels();
		ItemRegistry.initModels();
	}
	
	@SubscribeEvent
	public void loadWorld(WorldEvent.Load evt)
	{
		if (evt.getWorld().isRemote) evt.getWorld().addEventListener(BlockUpdateListener.instance);
	}
}