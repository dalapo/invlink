package dalapo.invlink.block;

import dalapo.invlink.helper.BlockHelper;
import dalapo.invlink.reference.StateList;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.BlockStateContainer;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.EnumActionResult;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class BlockDirectional extends BlockBase
{
	public BlockDirectional(Material materialIn, String name)
	{
		super(materialIn, name);
	}
	
	@Override
	public BlockStateContainer createBlockState()
	{
		return new BlockStateContainer(this, StateList.directions);
	}
	
	@Override
	public IBlockState getStateForPlacement(World world, BlockPos pos, EnumFacing dir, float hitX, float hitY, float hitZ, int meta, EntityLivingBase placer)
	{
		return getDefaultState().withProperty(StateList.directions, placer.isSneaking() ? dir.getOpposite() : dir);
	}
	
	@Override
	public boolean onBlockActivated(World world, BlockPos pos, IBlockState state, EntityPlayer ep, EnumHand hand, EnumFacing side, float hitX, float hitY, float hitZ)
	{
		if (ep.isSneaking() && ep.getHeldItem(hand).isEmpty())
		{
			world.setBlockState(pos, BlockHelper.rotateBlock(state));
			return true;
		}
		return super.onBlockActivated(world, pos, state, ep, hand, side, hitX, hitY, hitZ);
	}
	
	@Override
	public int getMetaFromState(IBlockState state)
	{
		return state.getValue(StateList.directions).ordinal();
	}
	
	@Override
	public IBlockState getStateFromMeta(int meta)
	{
		return getDefaultState().withProperty(StateList.directions, EnumFacing.getFront(meta % 6));
	}
}