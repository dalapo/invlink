package dalapo.invlink.block;

import dalapo.invlink.auxiliary.ISpecialItem;
import dalapo.invlink.client.render.PipeBakedModel;
import dalapo.invlink.client.render.PipeModelLoader;
import dalapo.invlink.helper.InvLinkMathHelper;
import dalapo.invlink.helper.Logger;
import dalapo.invlink.init.BlockRegistry;
import dalapo.invlink.item.ItemBase;
import dalapo.invlink.network.INetworkedObject;
import dalapo.invlink.reference.NameList;
import dalapo.invlink.reference.StateList;
import dalapo.invlink.tileentity.TileEntityDetectorPipe;
import dalapo.invlink.tileentity.TileEntityItemPipe;
import dalapo.invlink.tileentity.TileEntityTeleportPipe;
import net.minecraft.block.Block;
import net.minecraft.block.ITileEntityProvider;
import net.minecraft.block.material.Material;
import net.minecraft.block.properties.IProperty;
import net.minecraft.block.state.BlockStateContainer;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.block.model.ModelBakery;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.client.renderer.block.statemap.StateMapperBase;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.BlockRenderLayer;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.NonNullList;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.property.ExtendedBlockState;
import net.minecraftforge.common.property.IUnlistedProperty;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import net.minecraftforge.items.CapabilityItemHandler;
import net.minecraftforge.items.IItemHandler;

import static net.minecraftforge.items.CapabilityItemHandler.ITEM_HANDLER_CAPABILITY;

public class BlockPipeSection extends BakedModelBlock implements ITileEntityProvider, ISpecialItem
{
	private static final AxisAlignedBB PIPE_AABB = new AxisAlignedBB(0.25, 0.25, 0.25, 0.75, 0.75, 0.75);
	
	public BlockPipeSection(String name)
	{
		super(Material.IRON, name);
		setDefaultState(blockState.getBaseState().withProperty(StateList.pipetypes, StateList.PipeType.NORMAL));
		setCreativeTab(CreativeTabs.TRANSPORTATION);
		setHardness(4F);
		this.setLightOpacity(0);
	}

	@Override
	public TileEntity createNewTileEntity(World worldIn, int meta)
	{
		switch (meta)
		{
		case 0:
//			Logger.info("Creating regular pipe tile");
			return new TileEntityItemPipe();
		case 1:
		case 2:
//			Logger.info("Creating detector pipe tile");
			return new TileEntityDetectorPipe();
			default:
				throw new IllegalArgumentException("Invalid block metadata!");
		}
	}
	
	@Override
	public void observedNeighborChange(IBlockState state, World world, BlockPos pos, Block cause, BlockPos neighbour)
	{
		if (!world.isRemote)
		{
			TileEntity te = world.getTileEntity(neighbour);
			EnumFacing f = InvLinkMathHelper.getRelativeDirection(pos, neighbour);
			if (te != null && !(te instanceof TileEntityItemPipe))
			{
				TileEntityItemPipe pipe = (TileEntityItemPipe)world.getTileEntity(pos);
				if (te.hasCapability(ITEM_HANDLER_CAPABILITY, f.getOpposite()))
				{
					pipe.nonPipeNeighbourAdd(f, te.getCapability(ITEM_HANDLER_CAPABILITY, f.getOpposite()));
				}
				else if (pipe.getInventories().containsKey(f))
				{
					pipe.nonPipeNeighbourRemove(neighbour);
				}
			}
			else if (te == null || !(te instanceof TileEntityItemPipe || te.hasCapability(ITEM_HANDLER_CAPABILITY, f.getOpposite()))) // Block was removed
			{
				Logger.info("Removing neighbour");
				((TileEntityItemPipe)world.getTileEntity(pos)).nonPipeNeighbourRemove(neighbour);
			}
		}
	}
//	@Override
//	public boolean onBlockActivated(World world, BlockPos pos, IBlockState state, EntityPlayer ep, EnumHand hand, EnumFacing facing, float hitX, float hitY, float hitZ)
//	{
//		if (!world.isRemote && hand == EnumHand.MAIN_HAND)
//		{
//			TileEntityItemPipe pipe = (TileEntityItemPipe)world.getTileEntity(pos);
//			ItemStack is = ep.getHeldItem(hand);
//			if (!(is.getItem() instanceof ItemBase)) // Don't do this for debug items
//			{
//				pipe.getNetwork().printValidDestinations(is);
//				return true;
//			}
//		}
//		return false;
//	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public void initModel()
	{
		StateMapperBase ignoreState = new StateMapperBase()
		{
			@Override
			protected ModelResourceLocation getModelResourceLocation(IBlockState state)
			{
				switch (state.getValue(StateList.pipetypes))
				{
				case NORMAL:
					return PipeBakedModel.PIPE_NORMAL;
				case DETECTOR_OFF:
					return PipeBakedModel.PIPE_DETECTOR_OFF;
				case DETECTOR_ON:
					return PipeBakedModel.PIPE_DETECTOR_ON;
					default:
						return PipeBakedModel.PIPE_NORMAL;
				}
			}
		};
		ModelLoader.setCustomStateMapper(this, ignoreState);
	}
	
	@Override
	public IBlockState getStateFromMeta(int meta)
	{
		return getDefaultState().withProperty(StateList.pipetypes, StateList.PipeType.getFromMeta(meta));
	}
	
	@Override
	public int getMetaFromState(IBlockState state)
	{
		return state.getValue(StateList.pipetypes).ordinal();
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public void getSubBlocks(CreativeTabs tab, NonNullList<ItemStack> stacks)
	{
		if (tab.equals(CreativeTabs.TRANSPORTATION))
		{
			stacks.add(new ItemStack(this, 1, 0)); // regular pipe
			stacks.add(new ItemStack(this, 1, 1)); // detector pipe
		}
	}
	
	// We probably don't need all of these but we also might so yeah
	@Override
    public boolean isBlockNormalCube(IBlockState blockState)
	{
        return false;
    }
	
	@Override
    public boolean isNormalCube(IBlockState blockState)
	{
        return false;
    }

    @Override
    public boolean isOpaqueCube(IBlockState blockState)
    {
        return false;
    }
    
    @Override
    public boolean isTranslucent(IBlockState state)
    {
    	return true;
    }
    
    @Override
    public boolean isFullBlock(IBlockState state)
    {
    	return false;
    }
    
    @Override
    public boolean isFullCube(IBlockState state)
    {
        return false;
    }
    
    @SideOnly(Side.CLIENT)
    public BlockRenderLayer getBlockLayer()
    {
        return BlockRenderLayer.CUTOUT;
    }
    
    @Override
    public BlockStateContainer createBlockState()
    {
    	IProperty[] listedProperties = new IProperty[1];
    	listedProperties[0] = StateList.pipetypes;
        IUnlistedProperty[] unlistedProperties = new IUnlistedProperty[] {NORTH, SOUTH, WEST, EAST, UP, DOWN};
        return new ExtendedBlockState(this, listedProperties, unlistedProperties);
    }
    
	@Override
	@SideOnly(Side.CLIENT)
	public void initItemModel()
	{
		Item itemBlock = Item.REGISTRY.getObject(new ResourceLocation(NameList.MOD_ID, "pipe"));
		ModelResourceLocation[] arr = new ModelResourceLocation[3];
		arr[0] = new ModelResourceLocation(NameList.MOD_ID + ":pipe");
		arr[1] = new ModelResourceLocation(NameList.MOD_ID + ":det_off");
		arr[2] = new ModelResourceLocation(NameList.MOD_ID + ":det_on"); // unnecessary but safe
		ModelBakery.registerItemVariants(Item.getItemFromBlock(this), arr);
		
		for (int i=0; i<arr.length; i++)
		{
			Minecraft.getMinecraft().getRenderItem().getItemModelMesher().register(Item.getItemFromBlock(this), i, arr[i]);
		}
	}
	
	@Override
	protected boolean neighbouringBlockMeetsCriteria(IBlockAccess world, BlockPos pos, EnumFacing f)
	{
		TileEntity te = world.getTileEntity(pos);
		return te != null && ((te instanceof INetworkedObject && ((INetworkedObject)te).canConnectToSide(f.getOpposite())) || te.hasCapability(CapabilityItemHandler.ITEM_HANDLER_CAPABILITY, f.getOpposite()));
	}
	
	@Override
	public int getWeakPower(IBlockState state, IBlockAccess world, BlockPos pos, EnumFacing side)
	{
		return state.getValue(StateList.pipetypes) == StateList.PipeType.DETECTOR_ON ? 15 : 0;
	}

	@Override
	public String getName(ItemStack is)
	{
		return this.getUnlocalizedName();
	}
}