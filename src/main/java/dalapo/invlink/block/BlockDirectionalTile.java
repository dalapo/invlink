package dalapo.invlink.block;

import dalapo.invlink.helper.BlockHelper;
import dalapo.invlink.init.TileRegistry;
import dalapo.invlink.tileentity.TileEntityBase;
import net.minecraft.block.ITileEntityProvider;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class BlockDirectionalTile extends BlockDirectional implements ITileEntityProvider
{
	public BlockDirectionalTile(Material materialIn, String name)
	{
		super(materialIn, name);
	}

	@Override
	public TileEntity createNewTileEntity(World worldIn, int meta)
	{
		return TileRegistry.getTileFromName(name);
	}
	
	@Override
	public boolean onBlockActivated(World world, BlockPos pos, IBlockState state, EntityPlayer ep, EnumHand hand, EnumFacing side, float hitX, float hitY, float hitZ)
	{
		if (super.onBlockActivated(world, pos, state, ep, hand, side, hitX, hitY, hitZ))
		{
			((TileEntityBase)world.getTileEntity(pos)).updateState();
			return true;
		}
		return false;
	}
}