package dalapo.invlink.block;

import java.util.HashMap;
import java.util.Map;

import dalapo.invlink.auxiliary.UnlistedPropertyBlockAvailable;
import net.minecraft.block.material.Material;
import net.minecraft.block.properties.IProperty;
import net.minecraft.block.state.BlockStateContainer;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.client.renderer.block.statemap.StateMapperBase;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockAccess;
import net.minecraftforge.common.property.ExtendedBlockState;
import net.minecraftforge.common.property.IExtendedBlockState;
import net.minecraftforge.common.property.IUnlistedProperty;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public abstract class BakedModelBlock extends BlockBase
{
	public static final UnlistedPropertyBlockAvailable NORTH = new UnlistedPropertyBlockAvailable("north");
    public static final UnlistedPropertyBlockAvailable SOUTH = new UnlistedPropertyBlockAvailable("south");
    public static final UnlistedPropertyBlockAvailable WEST = new UnlistedPropertyBlockAvailable("west");
    public static final UnlistedPropertyBlockAvailable EAST = new UnlistedPropertyBlockAvailable("east");
    public static final UnlistedPropertyBlockAvailable UP = new UnlistedPropertyBlockAvailable("up");
    public static final UnlistedPropertyBlockAvailable DOWN = new UnlistedPropertyBlockAvailable("down");
	
	public BakedModelBlock(Material materialIn, String name) 
	{
		super(materialIn, name);
	}

	@Override
    protected BlockStateContainer createBlockState()
	{
        IProperty[] listedProperties = new IProperty[0]; // no listed properties
        IUnlistedProperty[] unlistedProperties = new IUnlistedProperty[] {NORTH, SOUTH, WEST, EAST, UP, DOWN};
        return new ExtendedBlockState(this, listedProperties, unlistedProperties);
    }
	
	@Override
	public IBlockState getExtendedState(IBlockState state, IBlockAccess world, BlockPos pos)
	{
		IExtendedBlockState extendedstate = (IExtendedBlockState)state;
		boolean north = neighbouringBlockMeetsCriteria(world, pos.north(), EnumFacing.NORTH);
        boolean south = neighbouringBlockMeetsCriteria(world, pos.south(), EnumFacing.SOUTH);
        boolean west = neighbouringBlockMeetsCriteria(world, pos.west(), EnumFacing.WEST);
        boolean east = neighbouringBlockMeetsCriteria(world, pos.east(), EnumFacing.EAST);
        boolean up = neighbouringBlockMeetsCriteria(world, pos.up(), EnumFacing.UP);
        boolean down = neighbouringBlockMeetsCriteria(world, pos.down(), EnumFacing.DOWN);
		return extendedstate.withProperty(NORTH, north).withProperty(SOUTH, south).withProperty(EAST, east).withProperty(WEST, west).withProperty(UP, up).withProperty(DOWN, down);
	}
	
	protected abstract boolean neighbouringBlockMeetsCriteria(IBlockAccess world, BlockPos pos, EnumFacing f);
}