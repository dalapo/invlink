package dalapo.invlink.block;

import dalapo.invlink.InventoryLink;
import dalapo.invlink.auxiliary.IGuiTile;
import dalapo.invlink.init.BlockRegistry;
import dalapo.invlink.reference.NameList;
import dalapo.invlink.tileentity.TileEntityBase;
import dalapo.invlink.tileentity.TileEntityItemPipe;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class BlockBase extends Block
{
	public final String name;
	public BlockBase(Material materialIn, String name)
	{
		super(materialIn);
		this.name = name;
		setUnlocalizedName(NameList.MOD_ID + ":" + name);
		setRegistryName(name);
		setHardness(4F);
		BlockRegistry.addBlock(this);
	}

	public void neighborChanged(IBlockState state, World world, BlockPos pos, Block block, BlockPos fromPos)
	{
		TileEntity te = world.getTileEntity(pos);
		if (te instanceof TileEntityBase)
		{
			TileEntityBase tile = (TileEntityBase)te;
			
			if (world.isBlockPowered(pos) && !tile.isPowered)
			{
				tile.onRedstoneEdge();
				tile.isPowered = true;
			}
			else if (!world.isBlockPowered(pos) && tile.isPowered)
			{
				tile.isPowered = false;
			}
		}
	}
	
	@SideOnly(Side.CLIENT)
	public void initModel()
	{
		ModelLoader.setCustomModelResourceLocation(Item.getItemFromBlock(this), 0, new ModelResourceLocation(getRegistryName(), "inventory"));
	}
	
	@SideOnly(Side.CLIENT)
	public void initItemModel()
	{
		// NO-OP
	}
	
	public boolean onBlockActivated(World world, BlockPos pos, IBlockState state, EntityPlayer ep, EnumHand hand, EnumFacing side, float hitX, float hitY, float hitZ)
	{
		TileEntity te = world.getTileEntity(pos);
		if (te instanceof IGuiTile)
		{
			if (!world.isRemote)
			{
				ep.openGui(InventoryLink.instance, 0, world, pos.getX(), pos.getY(), pos.getZ());
			}
			return true;
		}
		return false;
	}

	@Override
	public void breakBlock(World world, BlockPos pos, IBlockState state)
	{
		TileEntity te = world.getTileEntity(pos);
		if (te instanceof TileEntityBase) ((TileEntityBase)te).remove();
		super.breakBlock(world, pos, state);
	}
}